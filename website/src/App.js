import { createTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import UserForm from './Pages/UserForm';
import Heading from './Components/Heading';
import Footer from './Components/Footer';
import './App.css';

const theme = createTheme({
  typography: {
    'fontFamily': "Poppins",
    'fontSize': 14
  },
  overrides: {
    MuiInputLabel: { // Name of the component ⚛️ / style sheet
      root: { // Name of the rule
        fontSize: "14px",
      }
    },
    MuiMenuItem: {
      root: {
        fontSize: "14px"
      }
    },
    MuiSelect: {
      root:{
        fontSize: "14px"
      }
    },
    MuiTextField: {
      root: {
        background: "#F4F4F4",
        borderRadius: "8px",
        marginTop: "7px",
        height: "48px",
        fontSize: "14px"
      }
    }
  }
});

function App() {
  return (
    <ThemeProvider theme={theme}>
      <div className="App">
        <header className="App-header">
          <Heading/>
          <UserForm/>
          <Footer/>
        </header>
      </div>
    </ThemeProvider>
  );
}

export default App;
