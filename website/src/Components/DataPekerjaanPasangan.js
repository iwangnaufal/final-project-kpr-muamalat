import React, { useState, useContext } from "react";
import { styled } from "@mui/material/styles";
import {
  Box,
  Grid,
  Radio,
  RadioGroup,
  FormControlLabel,
  FormControl,
  Divider,
} from "@mui/material";
import "../Styles/styles.css";
import { makeStyles } from "@material-ui/core/styles";
import { TextField, MenuItem } from "@material-ui/core";
import { multiStepContext } from "../StepContext";
import InputUnstyled from "@mui/core/InputUnstyled";
import clsx from "clsx";

const useStyles = makeStyles(() => ({
  noBorder: {
    border: "none",
  },
}));

const StyledInputElement = styled("input")(`
  width: calc(100% - 16px);
  font-family: Poppins;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 24px;
  background: rgb(243, 246, 249);
  border: 0px solid #E5E8EC;
  border-radius: 8px;
  // padding: 6px 10px;
  color: #20262D;
  height: 48px;
  background: #f4f4f4;
  padding-left: 16px;

  &:hover {
    background: #ede5ee;
  }

  &:focus {
    outline: none;
    background: #ede5ee;
  }
`);

const CustomInput = React.forwardRef(function CustomInput(props, ref) {
  return (
    <InputUnstyled
      components={{ Input: StyledInputElement }}
      {...props}
      ref={ref}
      select
    />
  );
});

const BpIcon = styled("span")(({ theme }) => ({
  borderRadius: "50%",
  width: "7px",
  height: "7px",
  border: "1.5px solid #bfbfbf",
  padding: "3px",
  "input:hover ~ &": {
    backgroundColor: "#ebebeb",
  },
  "input:disabled ~ &": {
    boxShadow: "none",
    background:
      theme.palette.mode === "dark"
        ? "rgba(57,75,89,.5)"
        : "rgba(206,217,224,.5)",
  },
}));

const BpCheckedIcon = styled(BpIcon)({
  top: "3.5px",
  left: "3.5px",
  backgroundImage:
    "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
  "&:before": {
    display: "block",
    width: "7.5px",
    height: "7.5px",
    backgroundImage: "radial-gradient(#500878,#500878,#500878)",
    borderRadius: "50%",
    content: '""',
  },
  "input:hover ~ &": {
    // backgroundColor: '#106ba3',
  },
});

function BpRadio(props) {
  return (
    <Radio
      sx={{
        "&:hover": {
          bgcolor: "transparent",
        },
      }}
      disableRipple
      color="default"
      checkedIcon={<BpCheckedIcon />}
      icon={<BpIcon />}
      {...props}
    />
  );
}

export default function DataPekerjaanPasangan() {
  const classes = useStyles();
  const { setStepDataDiri, userData, setUserData } =
    useContext(multiStepContext);

  ///////////////////////////////////////////////////////////////////////////////////////////////////
  const [jenisPekerjaanPasangan, setJenisPekerjaanPasangan] = useState("");
  const [isJenisPekerjaanLainnyaPasangan, setIsJenisPekerjaanLainnyaPasangan] =
    useState(false);
  const [jenisPekerjaanLainnyaPasangan, setJenisPekerjaanLainnyaPasangan] =
    useState("");
  const [statusPekerjaanPasangan, setStatusPekerjaanPasangan] = useState("");
  const [isKaryawan, setIsKaryawan] = useState(false);
  ///////////////////////////////////////////////////////////////////////////////////////////////////
  const [namaPerusahaanPasangan, setNamaPerusahaanPasangan] = useState("");
  ///////////////////////////////////////////////////////////////////////////////////////////////////
  const [
    kategoriInstansiPekerjaanPasangan,
    setKategoriInstansiPekerjaanPasangan,
  ] = useState("");
  const [isPekerjaanLainnyaPasangan, setIsPekerjaanLainnyaPasangan] =
    useState(false);
  const [lainKategoriPekerjaanPasangan, setLainKategoriPekerjaanPasangan] =
    useState("");
  ///////////////////////////////////////////////////////////////////////////////////////////////////
  const [jabatanPasangan, setJabatanPasangan] = useState("");
  const [isJabatanLainnyaPasangan, setIsJabatanLainnyaPasangan] =
    useState(false);
  const [jabatanLainnyaPasangan, setJabatanLainnyaPasangan] = useState("");
  ///////////////////////////////////////////////////////////////////////////////////////////////////
  const [tahunLamaBekerjaPasangan, setTahunLamaBekerjaPasangan] = useState("");
  const [bulanLamaBekerjaPasangan, setBulanLamaBekerjaPasangan] = useState("");
  ///////////////////////////////////////////////////////////////////////////////////////////////////
  const [pendapatanBulananPasangan, setPendapatanBulananPasangan] =
    useState("");
  ///////////////////////////////////////////////////////////////////////////////////////////////////
  const [penghasilanTidakTetapPasangan, setPenghasilanTidakTetapPasangan] =
    useState("");
  ///////////////////////////////////////////////////////////////////////////////////////////////////
  const [pengeluaranTetapPasangan, setPengeluaranTetapPasangan] = useState("");
  ///////////////////////////////////////////////////////////////////////////////////////////////////
  const [informasiPenghasilanPasangan, setInformasiPenghasilanPasangan] =
    useState("");
  const [
    isInformasiLainnyaPenghasilanTambahanPasangan,
    setIsInformasiLainnyaPenghasilanTambahanPasangan,
  ] = useState(false);
  const [
    informasiLainnyaPenghasilanTambahanPasangan,
    setInformasiLainnyaPenghasilanTambahanPasangan,
  ] = useState("");
  const [isEmpty, setIsEmpty] = useState(true);
  ///////////////////////////////////////////////////////////////////////////////////////////////////

  const handleJenisPekerjaanPasanganKaryawan = (e) => {
    if (e === "Karyawan") {
      setIsKaryawan(true);
      setJenisPekerjaanPasangan(e);
    } else {
      setIsKaryawan(false);
      setJenisPekerjaanPasangan(e);
    }
  };

  const handlePekerjaanLainnyaPasangan = (e) => {
    console.log("ini kategori pekerjaan pasangan: ", e);
    if (e === "Lainnya") {
      setIsPekerjaanLainnyaPasangan(true);
      setKategoriInstansiPekerjaanPasangan(e);
    } else {
      setIsPekerjaanLainnyaPasangan(false);
      setKategoriInstansiPekerjaanPasangan(e);
    }
  };

  const handleJabatanLainnyaPasangan = (e) => {
    console.log("ini jabatan pasangan: ", e);

    if (e === "Lainnya") {
      setIsJabatanLainnyaPasangan(true);
      console.log("ini masuk ke true", isJabatanLainnyaPasangan);
      // setJabatanPemohon(event.target.value)
    } else {
      console.log("ini masuk ke false");
      setIsJabatanLainnyaPasangan(false);
      setJabatanPasangan(e);
    }
  };

  const handleInformasiLainnyaPenghasilanTambahanPasangan = (e) => {
    console.log("ini informasi penghasilan tambahan pasangan: ", e);

    if (e === "Lainnya") {
      setIsInformasiLainnyaPenghasilanTambahanPasangan(true);
      setInformasiPenghasilanPasangan(e);
    } else {
      console.log("ini masuk ke false");
      setIsInformasiLainnyaPenghasilanTambahanPasangan(false);
      setInformasiPenghasilanPasangan(e);
    }
  };

  return (
    <Box
      sx={{
        width: "80%",
        bgcolor: "#fff",
        alignItems: "center",
        paddingTop: "48px",
        paddingBottom: "48px",
        borderRadius: "34px",
      }}
    >
      <Box
        sx={{
          width: "70%",
          marginLeft: "15%",
        }}
      >
        <h2 className="titleOne">Data Pekerjaan</h2>
        <Divider
          sx={{
            marginTop: "24px",
            border: "0, 1px solid #363636",
            backgroundColor: "#363636",
          }}
        />
        <h3 className="titleTwo">Data Pekerjaan Pasangan</h3>

        <label className="basicLabel">Jenis Pekerjaan</label>
        <TextField
          variant="outlined"
          margin="normal"
          label={jenisPekerjaanPasangan === "" ? "Pilih Jenis Pekerjaan" : ""}
          InputLabelProps={{ shrink: false }}
          defaultValue={jenisPekerjaanPasangan}
          onChange={(e) => handleJenisPekerjaanPasanganKaryawan(e.target.value)}
          // required
          fullWidth
          InputProps={{
            classes: { notchedOutline: classes.noBorder },
          }}
          select
        >
          <MenuItem value="Karyawan">Karyawan</MenuItem>
          <MenuItem value="Self Employee">Self Employee</MenuItem>
          <MenuItem value="Professional">Professional</MenuItem>
        </TextField>
        {isKaryawan ? (
          <div>
            <label className="basicLabel">Status Pekerjaan</label>
            <RadioGroup
              aria-label="gender"
              name="controlled-radio-buttons-group"
              sx={{
                width: "100%",
              }}
            >
              <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                  <FormControlLabel
                    value="Karyawan Tetap"
                    control={<BpRadio />}
                    label={
                      <span style={{ fontFamily: "Poppins", fontSize: "14px" }}>
                        {"Karyawan Tetap"}
                      </span>
                    }
                    sx={{
                      backgroundColor: "#f4f4f4",
                      borderRadius: "8px",
                      height: "48px",
                      marginLeft: "0px",
                      paddingLeft: "0px",
                      width: "100%",
                      color: "#363636",
                    }}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <FormControlLabel
                    value="Karyawan Kontrak"
                    control={<BpRadio />}
                    label={
                      <span style={{ fontFamily: "Poppins", fontSize: "14px" }}>
                        {"Karyawan Kontrak"}
                      </span>
                    }
                    sx={{
                      backgroundColor: "#f4f4f4",
                      borderRadius: "8px",
                      height: "48px",
                      marginLeft: "0px",
                      paddingLeft: "0px",
                      width: "100%",
                      color: "#363636",
                    }}
                  />
                </Grid>
              </Grid>
            </RadioGroup>
          </div>
        ) : null}

        <label className="basicLabel">Nama Perusahaan</label>
        <CustomInput
          // aria-label="Atas Nama Kepemilikan"
          placeholder="Masukkan Nama Perusahaan"
        />

        <label className="basicLabel">Kategori Instansi</label>
        <TextField
          variant="outlined"
          margin="normal"
          label={
            kategoriInstansiPekerjaanPasangan === ""
              ? "Pilih Kategori Instansi"
              : ""
          }
          InputLabelProps={{ shrink: false }}
          defaultValue={kategoriInstansiPekerjaanPasangan}
          onChange={(e) => handlePekerjaanLainnyaPasangan(e.target.value)}
          // required
          fullWidth
          InputProps={{
            classes: { notchedOutline: classes.noBorder },
          }}
          select
        >
          <MenuItem value="Karyawan">Pemerintahan</MenuItem>
          <MenuItem value="BUMN">BUMN</MenuItem>
          <MenuItem value="Swasta Asing">Swasta Asing</MenuItem>
          <MenuItem value="Swasta Nasional">Swasta Nasional</MenuItem>
          <MenuItem value="TNI/Polri">TNI/Polri</MenuItem>
          <MenuItem value="Lainnya">Lainnya</MenuItem>
        </TextField>
        {isPekerjaanLainnyaPasangan ? (
          <div>
            <label className="basicLabel">Lainnya</label>
            <CustomInput
              // aria-label="Atas Nama Kepemilikan"
              placeholder="Masukkan Kategori Instansi"
            />
          </div>
        ) : null}

        <label className="basicLabel">Jabatan</label>
        <TextField
          variant="outlined"
          margin="normal"
          label={jabatanPasangan === "" ? "Pilih Jabatan" : ""}
          InputLabelProps={{ shrink: false }}
          defaultValue={jabatanPasangan}
          onChange={(e) => handleJabatanLainnyaPasangan(e.target.value)}
          // required
          fullWidth
          InputProps={{
            classes: { notchedOutline: classes.noBorder },
          }}
          select
        >
          <MenuItem value="Staff">Staff</MenuItem>
          <MenuItem value="Supervisor">Supervisor</MenuItem>
          <MenuItem value="Manager">Manager</MenuItem>
          <MenuItem value="Head/General Manager">Head/General Manager</MenuItem>
          <MenuItem value="Direktur">Direktur</MenuItem>
          <MenuItem value="Komisaris">Komisaris</MenuItem>
          <MenuItem value="Lainnya">Lainnya</MenuItem>
        </TextField>
        {isJabatanLainnyaPasangan ? (
          <div>
            <label className="basicLabel">Lainnya</label>
            <CustomInput
              // aria-label="Atas Nama Kepemilikan"
              placeholder="Masukkan Jabatan"
            />
          </div>
        ) : null}

        <Grid container spacing={2}>
          <Grid item xs={12} sm={6}>
            <label className="basicLabel">Lama Bekerja</label>
            <div className="inputWithIconRightWrapper">
              <input
                className="inputWithIconRight"
                type="number"
                placeholder="0"
              // onChange={(e) => setLuas_Bangunan(e.target.value)}
              />
              <label className="iconRight">Tahun</label>
            </div>
          </Grid>
          <Grid item xs={12} sm={6}>
            <label className="basicLabel">Lama Bekerja</label>
            <div className="inputWithIconRightWrapper">
              <input
                className="inputWithIconRight"
                type="number"
                placeholder="0"
              // onChange={(e) => setLuas_Bangunan(e.target.value)}
              />
              <label className="iconRight">Bulan</label>
            </div>
          </Grid>
        </Grid>

        <label className="basicLabel">Pendapatan Per Bulan</label>
        <div className="inputWithIconLeftWrapper">
          <input
            className="inputWithIconLeft"
            type="number"
            placeholder="0"
          />
          <label className="iconLeft">Rp</label>
        </div>

        <label className="basicLabel">
          Penghasilan Tidak Tetap Per Bulan (Opsional)
        </label>
        <TextField
          variant="outlined"
          margin="normal"
          label={
            penghasilanTidakTetapPasangan === ""
              ? "Pilih Penghasilan Tidak Tetap Per Bulan"
              : ""
          }
          InputLabelProps={{ shrink: false }}
          defaultValue={penghasilanTidakTetapPasangan}
          onChange={(e) => setPenghasilanTidakTetapPasangan(e.target.value)}
          // required
          fullWidth
          InputProps={{
            classes: { notchedOutline: classes.noBorder },
          }}
          select
        >
          <MenuItem value="< 10 Juta">Kurang dari 10 Juta</MenuItem>
          <MenuItem value="10-50 Juta">10-50 Juta</MenuItem>
          <MenuItem value="> 50 Juta">Lebih dari 50 Juta</MenuItem>
        </TextField>

        <label className="basicLabel">Pengeluaran Tetap Per Bulan</label>
        <TextField
          variant="outlined"
          margin="normal"
          label={
            pengeluaranTetapPasangan === ""
              ? "Pilih Pengeluaran Tetap Per Bulan"
              : ""
          }
          InputLabelProps={{ shrink: false }}
          defaultValue={pengeluaranTetapPasangan}
          onChange={(e) => setPengeluaranTetapPasangan(e.target.value)}
          // required
          fullWidth
          InputProps={{
            classes: { notchedOutline: classes.noBorder },
          }}
          select
        >
          <MenuItem value="< 10 Juta">Kurang dari 10 Juta</MenuItem>
          <MenuItem value="10-50 Juta">10-50 Juta</MenuItem>
          <MenuItem value="> 50 Juta">Lebih dari 50 Juta</MenuItem>
        </TextField>

        <label className="basicLabel">
          Informasi Penghasilan Tambahan (Opsional)
        </label>
        <TextField
          variant="outlined"
          margin="normal"
          label={
            informasiPenghasilanPasangan === ""
              ? "Pilih Informasi Penghasilan Tambahan"
              : ""
          }
          InputLabelProps={{ shrink: false }}
          defaultValue={informasiPenghasilanPasangan}
          onChange={(e) =>
            handleInformasiLainnyaPenghasilanTambahanPasangan(e.target.value)
          }
          // required
          fullWidth
          InputProps={{
            classes: { notchedOutline: classes.noBorder },
          }}
          select
        >
          <MenuItem value="Kerja Paruh Waktu">Kerja Paruh Waktu</MenuItem>
          <MenuItem value="Hasil Usaha">Hasil Usaha</MenuItem>
          <MenuItem value="Hasil Sewa">Hasil Sewa</MenuItem>
          <MenuItem value="Deviden">Deviden</MenuItem>
          <MenuItem value="Investasi">Investasi</MenuItem>
          <MenuItem value="Warisan">Warisan</MenuItem>
          <MenuItem value="Lainnya">Lainnya</MenuItem>
        </TextField>
        {isInformasiLainnyaPenghasilanTambahanPasangan ? (
          <div>
            <label className="basicLabel">Lainnya</label>
            <CustomInput
              // aria-label="Atas Nama Kepemilikan"
              placeholder="Masukkan  Informasi Penghasilan Tambahan"
            />
          </div>
        ) : null}
        <div className="firstPageButtonsWrapper">
          <div className="">
            {/* <input
              className="transparentButton"
              type="submit"
              value="Simpan Formulir"
            /> */}
          </div>
          <div className="sliceForSecondPageButton">
            <input
              className="secondaryButton"
              type="submit"
              value="Kembali"
              onClick={() => setStepDataDiri(5.1)}
            />
            <input
              className="primaryButton"
              type="submit"
              value="Lanjut"
              onClick={() => setStepDataDiri(5.4)}
            />
          </div>
        </div>
      </Box>
    </Box>
  );
}
