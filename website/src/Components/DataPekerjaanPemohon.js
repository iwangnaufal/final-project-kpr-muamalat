import React, { useState, useContext } from "react";
import { styled } from '@mui/material/styles';
import { Box, Grid, Radio, RadioGroup, FormControlLabel, FormControl, Divider } from '@mui/material'
import '../Styles/styles.css'
import { makeStyles } from "@material-ui/core/styles";
import { TextField, MenuItem } from '@material-ui/core';
import { multiStepContext } from "../StepContext"

const useStyles = makeStyles(() => ({
  noBorder: {
    border: "none",
  }
}));

const BpIcon = styled("span")(({ theme }) => ({
  borderRadius: "50%",
  width: "7px",
  height: "7px",
  border: "1.5px solid #bfbfbf",
  padding: "3px",
  "input:hover ~ &": {
    backgroundColor: "#ebebeb"
  },
  "input:disabled ~ &": {
    boxShadow: "none",
    background:
      theme.palette.mode === "dark"
        ? "rgba(57,75,89,.5)"
        : "rgba(206,217,224,.5)"
  }
}));

const BpCheckedIcon = styled(BpIcon)({
  top: "3.5px",
  left: "3.5px",
  backgroundImage:
    "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
  "&:before": {
    display: "block",
    width: "7.5px",
    height: "7.5px",
    backgroundImage: "radial-gradient(#500878,#500878,#500878)",
    borderRadius: "50%",
    content: '""'
  },
  "input:hover ~ &": {
    // backgroundColor: '#106ba3',
  }
});

function BpRadio(props) {
  return (
    <Radio
      sx={{
        "&:hover": {
          bgcolor: "transparent"
        }
      }}
      disableRipple
      color="default"
      checkedIcon={<BpCheckedIcon />}
      icon={<BpIcon />}
      {...props}
    />
  );
}

export default function DataPekerjaanPemohon() {
  const classes = useStyles();
  const [value, setValue] = React.useState('female');
  const { setStepDataDiri, userData, setUserData } = useContext(multiStepContext);

  const [peruntukan_pembiayaan, setPeruntukan_Pembiayaan] = useState("")
  const [program, setProgram] = useState("")
  const [namaKerabat, setNamaKerabat] = useState("")

  const [jenisPekerjaanPemohon, setJenisPekerjaanPemohon] = useState("");
  const [isKaryawan, setIsKaryawan] = useState(false)
  const [statusPekerjaanPemohon, setStatusPekerjaanPemohon] = useState("");
  const [namaPerusahaanPemohon, setNamaPerusahaanPemohon] = useState("");
  const [jabatanPemohon, setJabatanPemohon] = useState("");
  const [isJabatanLainnyaPemohon, setIsJabatanLainnyaPemohon] = useState(false);
  const [jabatanLainnyaPemohon, setJabatanLainnyaPemohon] = useState("");
  const [kategoriInstansiPekerjaanPemohon, setKategoriInstansiPekerjaanPemohon] = useState("");
  const [isPekerjaanLainnyaPemohon, setIsPekerjaanLainnyaPemohon] = useState(false);
  const [lainKategoriPekerjaanPemohon, setLainKategoriPekerjaanPemohon] = useState("");
  const [tahunLamaBekerjaPemohon, setTahunLamaBekerjaPemohon] = useState("");
  const [bulanLamaBekerjaPemohon, setBulanLamaBekerjaPemohon] = useState("");
  const [pendapatanBulananPemohon, setPendapatanBulananPemohon] = useState("");
  const [penghasilanTidakTetapPemohon, setPenghasilanTidakTetapPemohon] = useState("");
  const [pengeluaranTetapPemohon, setPengeluaranTetapPemohon] = useState("");
  const [informasiPenghasilanPemohon, setInformasiPenghasilanPemohon] = useState("");
  const [isInformasiLainnyaPenghasilanTambahanPemohon, setIsInformasiLainnyaPenghasilanTambahanPemohon] = useState(false);
  const [informasiLainnyaPenghasilanTambahanPemohon, setInformasiLainnyaPenghasilanTambahanPemohon] = useState("");
  const [isEmpty, setIsEmpty] = useState(true);
  const handleChange = (event) => {
    setValue(event.target.value);
  };

  const handlePekerjaanLainnyaPemohon = (e) => {
    console.log("ini kategori pekerjaan pemohon: ", e)
    if (e === "Lainnya") {
      setIsPekerjaanLainnyaPemohon(true)
      setKategoriInstansiPekerjaanPemohon(e)
    } else {
      setIsPekerjaanLainnyaPemohon(false)
      setKategoriInstansiPekerjaanPemohon(e)
    }
  }

  const handleJabatanLainnyaPemohon = (e) => {
    console.log("ini jabatan pemohon: ", e)

    if (e === "Lainnya") {
      setIsJabatanLainnyaPemohon(true)
      console.log("ini masuk ke true", isJabatanLainnyaPemohon)
      setJabatanPemohon(e)
    } else {
      console.log("ini masuk ke false")
      setIsJabatanLainnyaPemohon(false)
      setJabatanPemohon(e)
    }
  }

  const handleInformasiLainnyaPenghasilanTambahanPemohon = (e) => {
    console.log("ini informasi penghasilan tambahan pemohon: ", e)

    if (e === "Lainnya") {
      setIsInformasiLainnyaPenghasilanTambahanPemohon(true)
      setInformasiPenghasilanPemohon(e)
    } else {
      console.log("ini masuk ke false")
      setIsInformasiLainnyaPenghasilanTambahanPemohon(false)
      setInformasiPenghasilanPemohon(e)
    }
  }

  const handleJenisPekerjaanPemohonKaryawan = (e) => {
    if (e === "Karyawan") {
      setIsKaryawan(true)
      setJenisPekerjaanPemohon(e)
    }
    else {
      setIsKaryawan(false)
      setJenisPekerjaanPemohon(e)
    }
  }

  return (
    <Box sx={{
      width: '80%',
      bgcolor: '#fff',
      alignItems: 'center',
      paddingTop: '48px',
      paddingBottom: '48px',
      borderRadius: '34px'

    }}>
      <Box sx={{
        width: '70%',
        marginLeft: '15%'
      }}>
        <h2 className="titleOne">Data Pekerjaan</h2>
        <Divider
          sx={{
            marginTop: '24px',
            border: '0, 1px solid #363636',
            backgroundColor: '#363636'
          }}
        />
        <h3 className="titleTwo">Data Pekerjaan Pemohon</h3>
        <FormControl component="fieldset" sx={{ width: '100%' }}>
          <label className="basicLabel">Jenis Pekerjaan</label>
          <TextField
            variant="outlined"
            margin="normal"
            label={jenisPekerjaanPemohon === "" ? "Pilih Jenis Pekerjaan" : ""}
            InputLabelProps={{ shrink: false }}
            defaultValue={jenisPekerjaanPemohon}
            onChange={(e) => handleJenisPekerjaanPemohonKaryawan(e.target.value)}
            // required
            fullWidth
            InputProps={{
              classes: { notchedOutline: classes.noBorder }
            }}
            select>
            <MenuItem value="Karyawan">Karyawan</MenuItem>
            <MenuItem value="Self Employee">Self Employee</MenuItem>
            <MenuItem value="Professional">Professional</MenuItem>
          </TextField>
          {
            isKaryawan
              ?
              <div>
                <label className="basicLabel">Status Pekerjaan</label>
                <RadioGroup
                  aria-label="gender"
                  name="controlled-radio-buttons-group"
                  defaultValue={statusPekerjaanPemohon}
                  onChange={(e) => setStatusPekerjaanPemohon(e.target.value)}
                  sx={{
                    width: '100%'
                  }}
                >
                  <Grid container spacing={2}>
                    <Grid item xs={12} sm={6}>
                      <FormControlLabel value="female" control={<BpRadio />}
                        label={<span style={{ fontFamily: 'Poppins', fontSize: '14px' }}>{"Karyawan Tetap"}</span>}
                        sx={{
                          backgroundColor: '#f4f4f4',
                          borderRadius: '8px',
                          height: '48px',
                          marginLeft: '0px',
                          paddingLeft: '0px',
                          width: '100%',
                          color: '#363636',
                        }}
                      />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <FormControlLabel value="male" control={<BpRadio />}
                        label={<span style={{ fontFamily: 'Poppins', fontSize: '14px' }}>{"Karyawan Kontrak"}</span>}
                        sx={{
                          backgroundColor: '#f4f4f4',
                          borderRadius: '8px',
                          height: '48px',
                          marginLeft: '0px',
                          paddingLeft: '0px',
                          width: '100%',
                          color: '#363636'
                        }}
                      />
                    </Grid>
                  </Grid>
                </RadioGroup>
              </div>
              : null
          }

          <label className="basicLabel">Nama Perusahaan</label>
          <TextField
            variant="outlined"
            margin="normal"
            label={namaPerusahaanPemohon === "" ? "Masukkan Nama Perusahaan" : ""}
            InputLabelProps={{ shrink: false }}
            defaultValue={namaPerusahaanPemohon}
            onChange={(e) => setNamaPerusahaanPemohon(e.target.value)}
            // required
            fullWidth
            InputProps={{
              classes: { notchedOutline: classes.noBorder }
            }}
          />

          <label className="basicLabel">Kategori Instansi</label>
          <TextField
            variant="outlined"
            margin="normal"
            label={kategoriInstansiPekerjaanPemohon === "" ? "Pilih Kategori Instansi" : ""}
            InputLabelProps={{ shrink: false }}
            defaultValue={kategoriInstansiPekerjaanPemohon}
            onChange={(e) => handlePekerjaanLainnyaPemohon(e.target.value)}
            // required
            fullWidth
            InputProps={{
              classes: { notchedOutline: classes.noBorder }
            }}
            select>
            <MenuItem value="Pemerintahan">Pemerintahan</MenuItem>
            <MenuItem value="BUMN">BUMN</MenuItem>
            <MenuItem value="Swasta Asing">Swasta Asing</MenuItem>
            <MenuItem value="Swasta Nasional">Swasta Nasional</MenuItem>
            <MenuItem value="TNI / Polri">TNI / Polri</MenuItem>
            <MenuItem value="Lainnya">Lainnya</MenuItem>
          </TextField>
          {
            isPekerjaanLainnyaPemohon ?
              <TextField
                variant="outlined"
                margin="normal"
                label={lainKategoriPekerjaanPemohon === "" ? "Lainnya" : ""}
                InputLabelProps={{ shrink: false }}
                defaultValue={lainKategoriPekerjaanPemohon}
                onChange={(e) => setLainKategoriPekerjaanPemohon(e.target.value)}
                // required
                fullWidth
                InputProps={{
                  classes: { notchedOutline: classes.noBorder }
                }}
              />
              : null
          }

          <label className="basicLabel">Jabatan</label>
          <TextField
            variant="outlined"
            margin="normal"
            label={jabatanPemohon === "" ? "Pilih Jabatan" : ""}
            InputLabelProps={{ shrink: false }}
            defaultValue={jabatanPemohon}
            onChange={(e) => handleJabatanLainnyaPemohon(e.target.value)}
            // required
            fullWidth
            InputProps={{
              classes: { notchedOutline: classes.noBorder }
            }}
            select>
            <MenuItem value="Staff">Staff</MenuItem>
            <MenuItem value="Supervisor">Supervisor</MenuItem>
            <MenuItem value="Manager">Manager</MenuItem>
            <MenuItem value="Head/General Manager">Head/General Manager</MenuItem>
            <MenuItem value="Direktur">Direktur</MenuItem>
            <MenuItem value="Lainnya">Lainnya</MenuItem>
          </TextField>
          {
            isJabatanLainnyaPemohon ?
              <TextField
                variant="outlined"
                margin="normal"
                label={jabatanLainnyaPemohon === "" ? "Lainnya" : ""}
                InputLabelProps={{ shrink: false }}
                defaultValue={jabatanLainnyaPemohon}
                onChange={(e) => setJabatanLainnyaPemohon(e.target.value)}
                // required
                fullWidth
                InputProps={{
                  classes: { notchedOutline: classes.noBorder }
                }}
              />
              : null
          }

          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <label className="basicLabel">Lama Bekerja</label>
              <div className="inputWithIconRightWrapper">
                <input
                  className="inputWithIconRight"
                  type="number"
                  placeholder="0"
                  onChange={(e) => setTahunLamaBekerjaPemohon(e.target.value)}
                  defaultValue={tahunLamaBekerjaPemohon}
                />
                <label className="iconRight">tahun</label>
              </div>
            </Grid>
            <Grid item xs={12} sm={6}>
              <label className="basicLabel">.</label>
              <div className="inputWithIconRightWrapper">
                <input
                  className="inputWithIconRight"
                  type="number"
                  placeholder="0"
                  onChange={(e) => setBulanLamaBekerjaPemohon(e.target.value)}
                  defaultValue={bulanLamaBekerjaPemohon}
                />
                <label className="iconRight">bulan</label>
              </div>
            </Grid>
          </Grid>
          <label className="basicLabel">Pendapatan Per Bulan</label>
          <div className="inputWithIconLeftWrapper">
            <input
              className="inputWithIconLeft"
              type="number"
              placeholder="0"
              onChange={(e) => setPendapatanBulananPemohon(e.target.value)}
              defaultValue={pendapatanBulananPemohon}
            />
            <label className="iconLeft">Rp</label>
          </div>

          <label className="basicLabel">Penghasilan Tidak Tetap Per Bulan (Opsional)</label>
          <TextField
            variant="outlined"
            margin="normal"
            label={penghasilanTidakTetapPemohon === "" ? "Pilih Penghasilan Tidak Tetap Per Bulan" : ""}
            InputLabelProps={{ shrink: false }}
            defaultValue={penghasilanTidakTetapPemohon}
            onChange={(e) => setPenghasilanTidakTetapPemohon(e.target.value)}
            // required
            fullWidth
            InputProps={{
              classes: { notchedOutline: classes.noBorder }
            }}
            select>
            <MenuItem value="<10 Juta">Kurang dari 10 Juta</MenuItem>
            <MenuItem value="10-50 Juta">10-50 Juta</MenuItem>
            <MenuItem value=">50 Juta">Lebih dari 50 Juta</MenuItem>
          </TextField>

          <label className="basicLabel">Pengeluaran Tetap Per Bulan</label>
          <TextField
            variant="outlined"
            margin="normal"
            label={pengeluaranTetapPemohon === "" ? "Pilih Pengeluaran Tetap Per Bulan" : ""}
            InputLabelProps={{ shrink: false }}
            defaultValue={pengeluaranTetapPemohon}
            onChange={(e) => setPengeluaranTetapPemohon(e.target.value)}
            // required
            fullWidth
            InputProps={{
              classes: { notchedOutline: classes.noBorder }
            }}
            select>
            <MenuItem value="<10 Juta">Kurang dari 10 Juta</MenuItem>
            <MenuItem value="10-50 Juta">10-50 Juta</MenuItem>
            <MenuItem value=">50 Juta">Lebih dari 50 Juta</MenuItem>
          </TextField>

          <label className="basicLabel">Informasi Penghasilan Tambahan (Opsional)</label>
          <TextField
            variant="outlined"
            margin="normal"
            label={informasiPenghasilanPemohon === "" ? "Pilih Informasi Penghasilan Tambahan" : ""}
            InputLabelProps={{ shrink: false }}
            defaultValue={informasiPenghasilanPemohon}
            onChange={(e) => handleInformasiLainnyaPenghasilanTambahanPemohon(e.target.value)}
            // required
            fullWidth
            InputProps={{
              classes: { notchedOutline: classes.noBorder }
            }}
            select>
            <MenuItem value="Kerja Paruh Waktu">Kerja Paruh Waktu</MenuItem>
            <MenuItem value="Hasil Usaha">Hasil Usaha</MenuItem>
            <MenuItem value="Hasil Sewa">Hasil Sewa</MenuItem>
            <MenuItem value="Deviden">Deviden</MenuItem>
            <MenuItem value="Investasi">Investasi</MenuItem>
            <MenuItem value="Warisan">Warisan</MenuItem>
            <MenuItem value="Lainnya">Lainnya</MenuItem>
          </TextField>
          {
            isInformasiLainnyaPenghasilanTambahanPemohon ?
              <TextField
                variant="outlined"
                margin="normal"
                label={jabatanLainnyaPemohon === "" ? "Lainnya" : ""}
                InputLabelProps={{ shrink: false }}
                defaultValue={informasiLainnyaPenghasilanTambahanPemohon}
                onChange={(e) => setInformasiLainnyaPenghasilanTambahanPemohon(e.target.value)}
                // required
                fullWidth
                InputProps={{
                  classes: { notchedOutline: classes.noBorder }
                }}
              />
              : null
          }
          <div className="firstPageButtonsWrapper">
            <div className="">
              {/* <input
                className="transparentButton"
                type="submit"
                value="Simpan Formulir"
              /> */}
            </div>
            <div className="sliceForSecondPageButton">
              <input
                className="secondaryButton"
                type="submit"
                value="Kembali"
                onClick={() => setStepDataDiri(4.2)}
              />
              <input
                className="primaryButton"
                type="submit"
                value="Lanjut"
                onClick={() => setStepDataDiri(5.2)}
              />
            </div>
          </div>
        </FormControl>
      </Box>
    </Box>
  )
}
