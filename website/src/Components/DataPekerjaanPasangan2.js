import React, { useState, useContext, useEffect } from "react";
import { styled } from "@mui/material/styles";
import {
  Box,
  Grid,
  Radio,
  RadioGroup,
  FormControlLabel,
  FormControl,
  Divider,
} from "@mui/material";
import "../Styles/styles.css";
import { makeStyles } from "@material-ui/core/styles";
import { TextField, MenuItem } from "@material-ui/core";
import { multiStepContext } from "../StepContext";
import InputUnstyled from "@mui/core/InputUnstyled";
import axios from "axios";

const useStyles = makeStyles(() => ({
  noBorder: {
    border: "none",
  },
}));

const StyledInputElement = styled("input")(`
  width: calc(100% - 16px);
  font-family: Poppins;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 24px;
  background: rgb(243, 246, 249);
  border: 0px solid #E5E8EC;
  border-radius: 8px;
  // padding: 6px 10px;
  color: #20262D;
  height: 48px;
  background: #f4f4f4;
  padding-left: 16px;

  &:hover {
    background: #ede5ee;
  }

  &:focus {
    outline: none;
    background: #ede5ee;
  }
`);

const CustomInput = React.forwardRef(function CustomInput(props, ref) {
  return (
    <InputUnstyled
      components={{ Input: StyledInputElement }}
      {...props}
      ref={ref}
      select
    />
  );
});

const BpIcon = styled("span")(({ theme }) => ({
  borderRadius: "50%",
  width: "7px",
  height: "7px",
  border: "1.5px solid #bfbfbf",
  padding: "3px",
  "input:hover ~ &": {
    backgroundColor: "#ebebeb",
  },
  "input:disabled ~ &": {
    boxShadow: "none",
    background:
      theme.palette.mode === "dark"
        ? "rgba(57,75,89,.5)"
        : "rgba(206,217,224,.5)",
  },
}));

const BpCheckedIcon = styled(BpIcon)({
  top: "3.5px",
  left: "3.5px",
  backgroundImage:
    "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
  "&:before": {
    display: "block",
    width: "7.5px",
    height: "7.5px",
    backgroundImage: "radial-gradient(#500878,#500878,#500878)",
    borderRadius: "50%",
    content: '""',
  },
  "input:hover ~ &": {
    // backgroundColor: '#106ba3',
  },
});

function BpRadio(props) {
  return (
    <Radio
      sx={{
        "&:hover": {
          bgcolor: "transparent",
        },
      }}
      disableRipple
      color="default"
      checkedIcon={<BpCheckedIcon />}
      icon={<BpIcon />}
      {...props}
    />
  );
}

export default function DataPekerjaanPasangan2() {
  const classes = useStyles();
  const { setStepDataDiri, userData, setUserData } =
    useContext(multiStepContext);

  ///////////////////////////////////////////////////////////////////////////////////////////////////
  const [noTeleponKantorPasangan, setNoTeleponKantorPasangan] = useState("");
  const [noTeleponHRDPasangan, setNoTeleponHRDPasangan] = useState("");
  const [alamatKantorPasangan, setAlamatKantorPasangan] = useState("");
  const [provinsiPasangan, setProvinsiPasangan] = useState("");
  const [kabKotaPasangan, setKabKotaPasangan] = useState("");
  const [kecamatanPasangan, setKecamatanPasangan] = useState("");
  const [kelurahanPasangan, setKelurahanPasangan] = useState("");
  const [kodePosPasangan, setKodePosPasangan] = useState("");
  const [isEmpty, setIsEmpty] = useState(true);
  ///////////////////////////////////////////////////////////////////////////////////////////////////
  // ALAMAT KTP
  const [getIdProvinsi, setGetIdProvinsi] = useState("");
  const [getIdKota, setGetIdKota] = useState("");
  const [getIdKecamatan, setGetIdKecamatan] = useState("");

  // ALAMAT KTP
  const [pilihanProvinsi, setPilihanProvinsi] = useState([]);
  const [pilihanKotaKabupaten, setPilihanKotaKabupaten] = useState([]);
  const [pilihanKecamatan, setPilihanKecamatan] = useState([]);
  const [pilihanKelurahan, setPilihanKelurahan] = useState([]);

  // CEK ALAMAT
  const cekDaerah = (idDaerah, tipeDaerah) => {
    const idOption = parseInt(idDaerah);

    if (tipeDaerah === "Provinsi") {
      pilihanProvinsi.forEach((value, index) => {
        if (idOption === value.id) {
          setProvinsiPasangan(value.nama);
        }
      });
    } else if (tipeDaerah === "Kab/Kota") {
      pilihanKotaKabupaten.forEach((value, index) => {
        if (idOption === value.id) {
          setKabKotaPasangan(value.nama);
        }
      });
    } else if (tipeDaerah === "Kecamatan") {
      pilihanKecamatan.forEach((value, index) => {
        if (idOption === value.id) {
          setKecamatanPasangan(value.nama);
        }
      });
    } else if (tipeDaerah === "Kelurahan") {
      pilihanKelurahan.forEach((value, index) => {
        if (idOption === value.id) {
          setKelurahanPasangan(value.nama);
        }
      });
    }
  };

  // ALAMAT KTP
  useEffect(() => {
    axios({
      url: "https://dev.farizdotid.com/api/daerahindonesia/provinsi",
      method: "GET",
    })
      .then((response) => {
        setPilihanProvinsi(response.data.provinsi);
      })
      .catch((err) => {
        console.log("error", err);
      });
  }, []);

  const pilihProvinsi = (getIdProvinsi) => {
    axios({
      url: `https://dev.farizdotid.com/api/daerahindonesia/kota?id_provinsi=${getIdProvinsi}`,
      method: "GET",
    })
      .then((response) => {
        setPilihanKotaKabupaten(response.data.kota_kabupaten);
      })
      .catch((err) => {
        console.log("error", err);
      });
  };

  const pilihKotaKabupaten = (getIdKota) => {
    axios({
      url: `https://dev.farizdotid.com/api/daerahindonesia/kecamatan?id_kota=${getIdKota}`,
      method: "GET",
    })
      .then((response) => {
        setPilihanKecamatan(response.data.kecamatan);
      })
      .catch((err) => {
        console.log("error", err);
      });
  };

  const pilihKecamatan = (getIdKecamatan) => {
    axios({
      url: `https://dev.farizdotid.com/api/daerahindonesia/kelurahan?id_kecamatan=${getIdKecamatan}`,
      method: "GET",
    })
      .then((response) => {
        setPilihanKelurahan(response.data.kelurahan);
      })
      .catch((err) => {
        console.log("error", err);
      });
  };

  return (
    <Box
      sx={{
        width: "80%",
        bgcolor: "#fff",
        alignItems: "center",
        paddingTop: "48px",
        paddingBottom: "48px",
        borderRadius: "34px",
      }}
    >
      <Box
        sx={{
          width: "70%",
          marginLeft: "15%",
        }}
      >
        <h2 className="titleOne">Data Pekerjaan</h2>
        <Divider
          sx={{
            marginTop: "24px",
            border: "0, 1px solid #363636",
            backgroundColor: "#363636",
          }}
        />
        <h3 className="titleTwo">Data Pekerjaan Pasangan</h3>

        <Grid container spacing={2}>
          <Grid item xs={12} sm={6}>
            <label className="basicLabel">No. Telepon Kantor</label>
            <TextField
              variant="outlined"
              margin="normal"
              label={
                noTeleponKantorPasangan === "" ? "Masukkan No. Telepon" : ""
              }
              InputLabelProps={{ shrink: false }}
              defaultValue={noTeleponKantorPasangan}
              onChange={(e) => setNoTeleponKantorPasangan(e.target.value)}
              // required
              fullWidth
              InputProps={{
                classes: { notchedOutline: classes.noBorder },
              }}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <label className="basicLabel">No. Telepon HRD</label>
            <div className="inputWithIconLeftWrapper">
              <input
                className="inputWithIconLeft"
                type="number"
                placeholder="81234567890"
                onChange={(e) => setNoTeleponHRDPasangan(e.target.value)}
                defaultValue={noTeleponHRDPasangan}
              />
              <label className="iconLeft">+62</label>
            </div>
          </Grid>
        </Grid>

        <label className="basicLabel">Alamat Kantor</label>
        <CustomInput
          // aria-label="Atas Nama Kepemilikan"
          placeholder="Masukkan Alamat Kantor"
        />

        <Grid container spacing={2}>
          <Grid item xs={12} sm={6}>
            <label className="basicLabel">Provinsi</label>
            <TextField
              variant="outlined"
              margin="normal"
              label={provinsiPasangan === "" ? "Pilih Provinsi" : ""}
              InputLabelProps={{ shrink: false }}
              defaultValue={provinsiPasangan}
              onChange={(e) => {
                setGetIdProvinsi(e.target.value);
                cekDaerah(e.target.value, "Provinsi");
              }}
              // required
              fullWidth
              InputProps={{
                classes: { notchedOutline: classes.noBorder },
              }}
              select
            >
              {pilihanProvinsi.map((provinsi, key) => {
                return (
                  <MenuItem key={key} value={provinsi.id}>
                    {provinsi.nama}
                  </MenuItem>
                );
              })}
            </TextField>
          </Grid>
          <Grid item xs={12} sm={6}>
            <label className="basicLabel">Kabupaten/Kota</label>
            <TextField
              variant="outlined"
              margin="normal"
              label={kabKotaPasangan === "" ? "Pilih Kab/Kota" : ""}
              InputLabelProps={{ shrink: false }}
              defaultValue={kabKotaPasangan}
              onClick={() => pilihProvinsi(getIdProvinsi)}
              onChange={(e) => {
                setGetIdKota(e.target.value);
                cekDaerah(e.target.value, "Kab/Kota");
              }}
              // required
              fullWidth
              InputProps={{
                classes: { notchedOutline: classes.noBorder },
              }}
              select
            >
              {pilihanKotaKabupaten.map((kota, key) => {
                return <MenuItem value={kota.id}>{kota.nama}</MenuItem>;
              })}
            </TextField>
          </Grid>
        </Grid>

        <Grid container spacing={2}>
          <Grid item xs={12} sm={6}>
            <label className="basicLabel">Kelurahan</label>
            <TextField
              variant="outlined"
              margin="normal"
              label={kelurahanPasangan === "" ? "Pilih Kelurahan" : ""}
              InputLabelProps={{ shrink: false }}
              defaultValue={kelurahanPasangan}
              onClick={() => pilihKecamatan(getIdKecamatan)}
              onChange={(e) => {
                cekDaerah(e.target.value, "Kelurahan");
              }}
              // required
              fullWidth
              InputProps={{
                classes: { notchedOutline: classes.noBorder },
              }}
              select
            >
              {pilihanKelurahan.map((kelurahan, key) => {
                return (
                  <MenuItem value={kelurahan.id}>{kelurahan.nama}</MenuItem>
                );
              })}
            </TextField>
          </Grid>
          <Grid item xs={12} sm={6}>
            <label className="basicLabel">Kecamatan</label>
            <TextField
              variant="outlined"
              margin="normal"
              label={kecamatanPasangan === "" ? "Pilih Kecamatan" : ""}
              InputLabelProps={{ shrink: false }}
              defaultValue={kecamatanPasangan}
              onClick={() => pilihKotaKabupaten(getIdKota)}
              onChange={(e) => {
                setGetIdKecamatan(e.target.value);
                cekDaerah(e.target.value, "Kecamatan");
              }}
              // required
              fullWidth
              InputProps={{
                classes: { notchedOutline: classes.noBorder },
              }}
              select
            >
              {pilihanKecamatan.map((kecamatan, key) => {
                return (
                  <MenuItem value={kecamatan.id}>{kecamatan.nama}</MenuItem>
                );
              })}
            </TextField>
          </Grid>
        </Grid>
        <Grid item xs={12} sm={6}>
          <label className="basicLabel">Kode Pos</label>
          <CustomInput aria-label="Provinsi" placeholder="Masukkan Kode Pos" />
        </Grid>
        <div className="firstPageButtonsWrapper">
          <div className="">
            {/* <input
              className="transparentButton"
              type="submit"
              value="Simpan Formulir"
            /> */}
          </div>
          <div className="sliceForSecondPageButton">
            <input
              className="secondaryButton"
              type="submit"
              value="Kembali"
              onClick={() => setStepDataDiri(5.3)}
            />
            <input
              className="primaryButton"
              type="submit"
              value="Lanjut"
            // onClick={() => setStepDataDiri(4.2)}
            />
          </div>
        </div>
      </Box>
    </Box>
  );
}
