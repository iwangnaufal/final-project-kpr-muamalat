import React, { useContext } from "react";
import { multiStepContext } from "../StepContext";

import "../Styles/Heading.css";
// import Profile from "./Profile";

function Heading() {
  const { stepDataProfile } = useContext(multiStepContext);

  const goToProfile = () => {
    console.log("masuk");
  }

  return (
    <div>
      <header id="headerUtama">
        <div id="headerWrapper">
          <div className="logoUtama">
            <img src="./assets/BMI.png" />
          </div>
        </div>
      </header>
    </div>
  );
}
export default Heading;
