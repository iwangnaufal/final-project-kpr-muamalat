import React, { useState, useContext, useEffect } from "react";
import { styled } from '@mui/material/styles';
import { Box, Grid, Radio, RadioGroup, FormControlLabel, FormControl, Divider } from '@mui/material'
import '../Styles/styles.css'
import { makeStyles } from "@material-ui/core/styles";
import { TextField, MenuItem } from '@material-ui/core';
import { multiStepContext } from "../StepContext";
import axios from "axios";

const useStyles = makeStyles(() => ({
  noBorder: {
    border: "none",
  }
}));

const BpIcon = styled("span")(({ theme }) => ({
  borderRadius: "50%",
  width: "7px",
  height: "7px",
  border: "1.5px solid #bfbfbf",
  padding: "3px",
  "input:hover ~ &": {
    backgroundColor: "#ebebeb"
  },
  "input:disabled ~ &": {
    boxShadow: "none",
    background:
      theme.palette.mode === "dark"
        ? "rgba(57,75,89,.5)"
        : "rgba(206,217,224,.5)"
  }
}));

const BpCheckedIcon = styled(BpIcon)({
  top: "3.5px",
  left: "3.5px",
  backgroundImage:
    "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
  "&:before": {
    display: "block",
    width: "7.5px",
    height: "7.5px",
    backgroundImage: "radial-gradient(#500878,#500878,#500878)",
    borderRadius: "50%",
    content: '""'
  },
  "input:hover ~ &": {
    // backgroundColor: '#106ba3',
  }
});

function BpRadio(props) {
  return (
    <Radio
      sx={{
        "&:hover": {
          bgcolor: "transparent"
        }
      }}
      disableRipple
      color="default"
      checkedIcon={<BpCheckedIcon />}
      icon={<BpIcon />}
      {...props}
    />
  );
}

export default function DataPekerjaanPemohon2() {
  const classes = useStyles();
  const [value, setValue] = React.useState('female');
  const { setStepDataDiri, userData, setUserData } = useContext(multiStepContext);

  const [peruntukan_pembiayaan, setPeruntukan_Pembiayaan] = useState("")
  const [program, setProgram] = useState("")
  const [namaKerabat, setNamaKerabat] = useState("")

  const [teleponKantorPemohon, setTeleponKantorPemohon] = useState("");
  const [teleponHRDPemohon, setTeleponHRDPemohon] = useState("");
  const [alamatKantorPemohon, setAlamatKantorPemohon] = useState("");
  const [provinsiKantorPemohon, setProvinsiKantorPemohon] = useState("");
  const [kabupatenKantorPemohon, setKabupatenKantorPemohon] = useState("");
  const [kecamatanKantorPemohon, setKecamatanKantorPemohon] = useState("");
  const [kelurahanKantorPemohon, setKelurahanKantorPemohon] = useState("");
  const [kodePosKantorPemohon, setKodePosKantorPemohon] = useState("");
  const [getIdProvinsi, setGetIdProvinsi] = useState("");
  const [getIdKota, setGetIdKota] = useState("");
  const [getIdKecamatan, setGetIdKecamatan] = useState("");
  const [getIdKelurahan, setGetIdKelurahan] = useState("");
  const [pilihanProvinsi, setPilihanProvinsi] = useState([]);
  const [pilihanKotaKabupaten, setPilihanKotaKabupaten] = useState([]);
  const [pilihanKecamatan, setPilihanKecamatan] = useState([]);
  const [pilihanKelurahan, setPilihanKelurahan] = useState([]);
  const [isEmpty, setIsEmpty] = useState(true)
  const handleChange = (event) => {
    setValue(event.target.value);
  };

  const cekDaerah = (idDaerah, tipeDaerah) => {
    const idOption = parseInt(idDaerah);

    if (tipeDaerah === "Provinsi") {
      pilihanProvinsi.forEach((value, index) => {
        if (idOption === value.id) {
          setProvinsiKantorPemohon(value.nama);
        }
      });
    } else if (tipeDaerah === "Kab/Kota") {
      pilihanKotaKabupaten.forEach((value, index) => {
        if (idOption === value.id) {
          setKabupatenKantorPemohon(value.nama);
        }
      });
    } else if (tipeDaerah === "Kecamatan") {
      pilihanKecamatan.forEach((value, index) => {
        if (idOption === value.id) {
          setKecamatanKantorPemohon(value.nama);
        }
      });
    } else if (tipeDaerah === "Kelurahan") {
      pilihanKelurahan.forEach((value, index) => {
        if (idOption === value.id) {
          setKelurahanKantorPemohon(value.nama);
        }
      });
    }
  };

  useEffect(() => {
    axios({
      url: "https://dev.farizdotid.com/api/daerahindonesia/provinsi",
      method: "GET",
    })
      .then((response) => {
        setPilihanProvinsi(response.data.provinsi);
      })
      .catch((err) => {
        console.log("error", err);
      });
  }, []);

  const pilihProvinsi = (getIdProvinsi) => {
    axios({
      url: `https://dev.farizdotid.com/api/daerahindonesia/kota?id_provinsi=${getIdProvinsi}`,
      method: "GET",
    })
      .then((response) => {
        setPilihanKotaKabupaten(response.data.kota_kabupaten);
      })
      .catch((err) => {
        console.log("error", err);
      });
  };

  const pilihKotaKabupaten = (getIdKota) => {
    axios({
      url: `https://dev.farizdotid.com/api/daerahindonesia/kecamatan?id_kota=${getIdKota}`,
      method: "GET",
    })
      .then((response) => {
        setPilihanKecamatan(response.data.kecamatan);
      })
      .catch((err) => {
        console.log("error", err);
      });
  };

  const pilihKecamatan = (getIdKecamatan) => {
    axios({
      url: `https://dev.farizdotid.com/api/daerahindonesia/kelurahan?id_kecamatan=${getIdKecamatan}`,
      method: "GET",
    })
      .then((response) => {
        setPilihanKelurahan(response.data.kelurahan);
      })
      .catch((err) => {
        console.log("error", err);
      });
  };

  return (
    <Box sx={{
      width: '80%',
      bgcolor: '#fff',
      alignItems: 'center',
      paddingTop: '48px',
      paddingBottom: '48px',
      borderRadius: '34px'

    }}>
      <Box sx={{
        width: '70%',
        marginLeft: '15%'
      }}>
        <h2 className="titleOne">Data Pekerjaan</h2>
        <Divider
          sx={{
            marginTop: '24px',
            border: '0, 1px solid #363636',
            backgroundColor: '#363636'
          }}
        />
        <h3 className="titleTwo">Data Pekerjaan Pemohon</h3>
        <FormControl component="fieldset" sx={{ width: '100%' }}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <label className="basicLabel">No. Telepon Kantor</label>
              <TextField
                variant="outlined"
                margin="normal"
                label={teleponKantorPemohon === "" ? "Masukkan No. Telepon" : ""}
                InputLabelProps={{ shrink: false }}
                defaultValue={teleponKantorPemohon}
                onChange={(e) => setTeleponKantorPemohon(e.target.value)}
                // required
                fullWidth
                InputProps={{
                  classes: { notchedOutline: classes.noBorder }
                }}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <label className="basicLabel">No. Telepon HRD</label>
              <div className="inputWithIconLeftWrapper">
                <input
                  className="inputWithIconLeft"
                  type="number"
                  placeholder="81234567890"
                  onChange={(e) => setTeleponHRDPemohon(e.target.value)}
                  defaultValue={teleponHRDPemohon}
                />
                <label className="iconLeft">+62</label>
              </div>
            </Grid>
          </Grid>

          <label className="basicLabel">Alamat Kantor</label>
          <TextField
            variant="outlined"
            margin="normal"
            label={alamatKantorPemohon === "" ? "Masukkan Alamat Kantor" : ""}
            InputLabelProps={{ shrink: false }}
            defaultValue={alamatKantorPemohon}
            onChange={(e) => setAlamatKantorPemohon(e.target.value)}
            // required
            fullWidth
            InputProps={{
              classes: { notchedOutline: classes.noBorder }
            }}
          />

          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <label className="basicLabel">Provinsi</label>
              <TextField
                variant="outlined"
                margin="normal"
                label={provinsiKantorPemohon === "" ? "Pilih Provinsi" : ""}
                InputLabelProps={{ shrink: false }}
                defaultValue={provinsiKantorPemohon}
                onChange={(e) => {
                  setGetIdProvinsi(e.target.value);
                  cekDaerah(e.target.value, "Provinsi");
                }}
                // required
                fullWidth
                InputProps={{
                  classes: { notchedOutline: classes.noBorder }
                }}
                select>
                {
                  pilihanProvinsi.map((provinsi, key) =>
                    <MenuItem
                      key={key}
                      value={provinsi.id}
                    >
                      {provinsi.nama}
                    </MenuItem>)
                }
              </TextField>
            </Grid>
            <Grid item xs={12} sm={6}>
              <label className="basicLabel">Kota/Kabupaten</label>
              <TextField
                variant="outlined"
                margin="normal"
                label={kabupatenKantorPemohon === "" ? "Pilih Kota/kabupaten" : ""}
                InputLabelProps={{ shrink: false }}
                defaultValue={kabupatenKantorPemohon}
                onClick={() => pilihProvinsi(getIdProvinsi)}
                onChange={(e) => {
                  setGetIdKota(e.target.value);
                  cekDaerah(e.target.value, "Kab/Kota");
                }}
                // required
                fullWidth
                InputProps={{
                  classes: { notchedOutline: classes.noBorder }
                }}
                select>
                {
                  pilihanKotaKabupaten.map((kota, key) =>
                    <MenuItem
                      key={key}
                      value={kota.id}
                    >
                      {kota.nama}
                    </MenuItem>)
                }
              </TextField>
            </Grid>
          </Grid>

          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <label className="basicLabel">Kecamatan</label>
              <TextField
                variant="outlined"
                margin="normal"
                label={kecamatanKantorPemohon === "" ? "Pilih Kecamatan" : ""}
                InputLabelProps={{ shrink: false }}
                defaultValue={kecamatanKantorPemohon}
                onClick={() => pilihKotaKabupaten(getIdKota)}
                onChange={(e) => {
                  setGetIdKecamatan(e.target.value);
                  cekDaerah(e.target.value, "Kecamatan");
                }}
                // required
                fullWidth
                InputProps={{
                  classes: { notchedOutline: classes.noBorder }
                }}
                select>
                {
                  pilihanKecamatan.map((kecamatan, key) =>
                    <MenuItem
                      key={key}
                      value={kecamatan.id}
                    >
                      {kecamatan.nama}
                    </MenuItem>)
                }
              </TextField>
            </Grid>
            <Grid item xs={12} sm={6}>
              <label className="basicLabel">Kelurahan</label>
              <TextField
                variant="outlined"
                margin="normal"
                label={kelurahanKantorPemohon === "" ? "Pilih Kelurahan" : ""}
                InputLabelProps={{ shrink: false }}
                defaultValue={kelurahanKantorPemohon}
                onClick={() => pilihKecamatan(getIdKecamatan)}
                onChange={(e) => {
                  setGetIdKelurahan(e.target.value);
                  cekDaerah(e.target.value, "Kelurahan");
                }}
                // required
                fullWidth
                InputProps={{
                  classes: { notchedOutline: classes.noBorder }
                }}
                select>
                {
                  pilihanKelurahan.map((kelurahan, key) =>
                    <MenuItem
                      key={key}
                      value={kelurahan.id}
                    >
                      {kelurahan.nama}
                    </MenuItem>)
                }
              </TextField>
            </Grid>
          </Grid>

          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              {/* sementara dibuat basic input dulu*/}
              <label className="basicLabel">Kode Pos</label>
              <TextField
                variant="outlined"
                margin="normal"
                label={kodePosKantorPemohon === "" ? "Pilih Kode Pos" : ""}
                InputLabelProps={{ shrink: false }}
                defaultValue={kodePosKantorPemohon}
                onChange={(e) => setKodePosKantorPemohon(e.target.value)}
                // required
                fullWidth
                InputProps={{
                  classes: { notchedOutline: classes.noBorder }
                }}
              />
            </Grid>
          </Grid>
          <div className="firstPageButtonsWrapper">
            <div className="">
              {/* <input
                className="transparentButton"
                type="submit"
                value="Simpan Formulir"
              /> */}
            </div>
            <div className="sliceForSecondPageButton">
              <input
                className="secondaryButton"
                type="submit"
                value="Kembali"
                onClick={() => setStepDataDiri(5.1)}
              />
              <input
                className="primaryButton"
                type="submit"
                value="Lanjut"
                onClick={() => setStepDataDiri(5.3)}
              />
            </div>
          </div>
        </FormControl>
      </Box>
    </Box>
  )
}
