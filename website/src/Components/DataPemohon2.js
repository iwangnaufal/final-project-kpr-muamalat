import React, { useState, useContext } from 'react';
import { styled } from '@mui/material/styles';
import { Box, Grid, Radio, RadioGroup, FormControlLabel, FormControl, Divider } from '@mui/material'
import '../Styles/styles.css'
import { makeStyles } from "@material-ui/core/styles";
import { TextField, MenuItem } from '@material-ui/core';
import { multiStepContext } from "../StepContext"


const useStyles = makeStyles(() => ({
    noBorder: {
        border: "none",
    }
}));


function FormDataDiriPemohonBagian2() {

    const { setStepDataDiri, userData, setUserData } = useContext(multiStepContext);

    const [alamatKtp, setAlamatKtp] = useState("");
    const [rt, setRt] = useState("");
    const [rw, setRw] = useState("");
    const [provinsi, setProvinsi] = useState("");
    const [kabupaten, setKabupaten] = useState("");
    const [kecamatan, setKecamatan] = useState("");
    const [kelurahan, setKelurahan] = useState("");
    const [kodePos, setKodePos] = useState("");

    const [alamatSaatIni, setAlamatSaatIni] = useState("");
    const [rtSaatIni, setRtSaatIni] = useState("");
    const [rwSaatIni, setRwSaatIni] = useState("");
    const [kelurahanSaatIni, setKelurahanSaatIni] = useState("");
    const [kecamatanSaatIni, setKecamatanSaatIni] = useState("");
    const [kabupatenSaatIni, setKabupatenSaatIni] = useState("");
    const [provinsiSaatIni, setProvinsiSaatIni] = useState("");
    const [kodePosSaatIni, setKodePosSaatIni] = useState("");
    const [noTlpRumah, setNoTlpRumah] = useState("");
    const [alamatSurat, setAlamatSurat] = useState("");

    const classes = useStyles();

    return (
        <Box sx={{
            width: '80%',
            bgcolor: '#fff',
            alignItems: 'center',
            paddingTop: '48px',
            paddingBottom: '48px',
            borderRadius: '34px'

        }}>
            <Box sx={{
                width: '70%',
                marginLeft: '15%'
            }}>

                <h2 className="titleOne">Data Pemohon</h2>
                <Divider
                    sx={{
                        marginTop: '24px',
                        border: '0, 1px solid #363636',
                        backgroundColor: '#363636'
                    }}
                />
                <FormControl component="fieldset" sx={{ width: '100%' }}>
                    <label className="basicLabel">Alamat Sesuai KTP</label>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        label={alamatKtp === "" ? "Masukkan Alamat Sesuai KTP" : ""}
                        InputLabelProps={{ shrink: false }}
                        type="text"
                        onChange={(e) => setAlamatKtp(e.target.value)}
                        defaultValue={alamatKtp}
                        // required
                        fullWidth
                        InputProps={{
                            classes: { notchedOutline: classes.noBorder }
                        }}
                    />
                    <Grid container spacing={3}>
                        <Grid item xs={6} sm={3}>
                            <label className="basicLabel">RT</label>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                label={rt === "" ? "000" : ""}
                                InputLabelProps={{ shrink: false }}
                                type="number"
                                onChange={(e) => setRt(e.target.value)}
                                defaultValue={rt}
                                // required
                                fullWidth
                                InputProps={{
                                    classes: { notchedOutline: classes.noBorder }
                                }}
                            />
                        </Grid>
                        <Grid item xs={6} sm={3}>
                            <label className="basicLabel">RW</label>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                // label={variabel3 === "" ? "Masukkan Label Grid 1" : ""}
                                InputLabelProps={{ shrink: false }}
                                type="number"
                                placeholder="000"
                                onChange={(e) => setRw(e.target.value)}
                                defaultValue={rw}
                                // required
                                fullWidth
                                InputProps={{
                                    classes: { notchedOutline: classes.noBorder }
                                }}
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <label className="basicLabel">Provinsi</label>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                label={provinsi === "" ? "Pilih Menu" : ""}
                                InputLabelProps={{ shrink: false }}
                                placeholder="Pilih Status Perkawinan"
                                placeholder="Pilih Provinsi"
                                onChange={(e) => setProvinsi(e.target.value)}
                                defaultValue={provinsi}
                                // required 
                                fullWidth
                                InputProps={{
                                    classes: { notchedOutline: classes.noBorder }
                                }}
                                select>
                                <MenuItem value="Value A">Value A</MenuItem>
                                <MenuItem value="Value B">Value B</MenuItem>
                                <MenuItem value="Value C">Value C</MenuItem>
                            </TextField>
                        </Grid>
                    </Grid>
                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={6}>
                            <label className="basicLabel">Kabupaten / Kota</label>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                label={kabupaten === "" ? "Pilih Menu" : ""}
                                InputLabelProps={{ shrink: false }}
                                placeholder="Pilih Kabupaten / Kota"
                                onChange={(e) => setKabupaten(e.target.value)}
                                defaultValue={kabupaten}
                                // required 
                                fullWidth
                                InputProps={{
                                    classes: { notchedOutline: classes.noBorder }
                                }}
                                select>
                                <MenuItem value="Value A">Value A</MenuItem>
                                <MenuItem value="Value B">Value B</MenuItem>
                                <MenuItem value="Value C">Value C</MenuItem>
                            </TextField>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <label className="basicLabel">Kecamatan</label>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                label={kecamatan === "" ? "Pilih Menu" : ""}
                                InputLabelProps={{ shrink: false }}
                                placeholder="Pilih Kecamatan"
                                onChange={(e) => setKecamatan(e.target.value)}
                                defaultValue={kecamatan}
                                // required 
                                fullWidth
                                InputProps={{
                                    classes: { notchedOutline: classes.noBorder }
                                }}
                                select>
                                <MenuItem value="Value A">Value A</MenuItem>
                                <MenuItem value="Value B">Value B</MenuItem>
                                <MenuItem value="Value C">Value C</MenuItem>
                            </TextField>
                        </Grid>
                    </Grid>
                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={6}>
                            <label className="basicLabel">Kelurahan</label>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                label={kelurahan === "" ? "Pilih Menu" : ""}
                                InputLabelProps={{ shrink: false }}
                                placeholder="Pilih Kelurahan"
                                onChange={(e) => setKelurahan(e.target.value)}
                                defaultValue={kelurahan}
                                // required 
                                fullWidth
                                InputProps={{
                                    classes: { notchedOutline: classes.noBorder }
                                }}
                                select>
                                <MenuItem value="Value A">Value A</MenuItem>
                                <MenuItem value="Value B">Value B</MenuItem>
                                <MenuItem value="Value C">Value C</MenuItem>
                            </TextField>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <label className="basicLabel">Kode Pos</label>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                label={kodePos === "" ? "Pilih Menu" : ""}
                                InputLabelProps={{ shrink: false }}
                                placeholder="Pilih Kode Pos"
                                onChange={(e) => setKodePos(e.target.value)}
                                defaultValue={kodePos}
                                // required 
                                fullWidth
                                InputProps={{
                                    classes: { notchedOutline: classes.noBorder }
                                }}
                                select>
                                <MenuItem value="Value A">Value A</MenuItem>
                                <MenuItem value="Value B">Value B</MenuItem>
                                <MenuItem value="Value C">Value C</MenuItem>
                            </TextField>
                        </Grid>
                    </Grid>
                    <br/>
                    <input type="checkbox"
                        name="Alamat Yang Sama"
                    />
                    <p style={{color:"black", fontSize:"12px", fontFamily:"Poppins"}}>Alamat Tinggal Saat ini sama dengan Alamat KTP</p>
                    <label className="basicLabel">Alamat Tinggal Saat Ini</label>

                    <TextField
                        variant="outlined"
                        margin="normal"
                        label={alamatSaatIni === "" ? "Masukkan Alamat Tinggal Saat Ini" : ""}
                        InputLabelProps={{ shrink: false }}
                        type="text"
                        onChange={(e) => setAlamatSaatIni(e.target.value)}
                        defaultValue={alamatSaatIni}
                        // required
                        fullWidth
                        InputProps={{
                            classes: { notchedOutline: classes.noBorder }
                        }}
                    />
                    <Grid container spacing={3}>
                        <Grid item xs={6} sm={3}>
                            <label className="basicLabel">RT</label>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                label={rtSaatIni === "" ? "000" : ""}
                                InputLabelProps={{ shrink: false }}
                                type="number"
                                onChange={(e) => setRtSaatIni(e.target.value)}
                                defaultValue={rt}
                                // required
                                fullWidth
                                InputProps={{
                                    classes: { notchedOutline: classes.noBorder }
                                }}
                            />
                        </Grid>
                        <Grid item xs={6} sm={3}>
                            <label className="basicLabel">RW</label>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                label={rwSaatIni === "" ? "000" : ""}
                                InputLabelProps={{ shrink: false }}
                                type="number"
                                onChange={(e) => setRwSaatIni(e.target.value)}
                                defaultValue={rwSaatIni}
                                // required
                                fullWidth
                                InputProps={{
                                    classes: { notchedOutline: classes.noBorder }
                                }}
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <label className="basicLabel">Provinsi</label>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                label={provinsiSaatIni === "" ? "Pilih Provinsi" : ""}
                                InputLabelProps={{ shrink: false }}
                                placeholder="Pilih Status Perkawinan"
                                onChange={(e) => setProvinsiSaatIni(e.target.value)}
                                defaultValue={provinsiSaatIni}
                                // required 
                                fullWidth
                                InputProps={{
                                    classes: { notchedOutline: classes.noBorder }
                                }}
                                select>
                                <MenuItem value="Value A">Value A</MenuItem>
                                <MenuItem value="Value B">Value B</MenuItem>
                                <MenuItem value="Value C">Value C</MenuItem>
                            </TextField>
                        </Grid>
                    </Grid>
                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={6}>
                            <label className="basicLabel">Kabupaten / Kota</label>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                label={kabupatenSaatIni === "" ? "Pilih Menu" : ""}
                                InputLabelProps={{ shrink: false }}
                                placeholder="Pilih Kabupaten / Kota"
                                onChange={(e) => setKabupatenSaatIni(e.target.value)}
                                defaultValue={kabupatenSaatIni}
                                // required 
                                fullWidth
                                InputProps={{
                                    classes: { notchedOutline: classes.noBorder }
                                }}
                                select>
                                <MenuItem value="Value A">Value A</MenuItem>
                                <MenuItem value="Value B">Value B</MenuItem>
                                <MenuItem value="Value C">Value C</MenuItem>
                            </TextField>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <label className="basicLabel">Kecamatan</label>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                label={kecamatanSaatIni === "" ? "Pilih Menu" : ""}
                                InputLabelProps={{ shrink: false }}
                                placeholder="Pilih Kecamatan"
                                onChange={(e) => setKecamatanSaatIni(e.target.value)}
                                defaultValue={kecamatanSaatIni}
                                // required 
                                fullWidth
                                InputProps={{
                                    classes: { notchedOutline: classes.noBorder }
                                }}
                                select>
                                <MenuItem value="Value A">Value A</MenuItem>
                                <MenuItem value="Value B">Value B</MenuItem>
                                <MenuItem value="Value C">Value C</MenuItem>
                            </TextField>
                        </Grid>
                    </Grid>
                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={6}>
                            <label className="basicLabel">Kelurahan</label>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                label={kelurahanSaatIni === "" ? "Pilih Menu" : ""}
                                InputLabelProps={{ shrink: false }}
                                placeholder="Pilih Kelurahan"
                                onChange={(e) => setKelurahanSaatIni(e.target.value)}
                                defaultValue={kelurahanSaatIni}
                                // required 
                                fullWidth
                                InputProps={{
                                    classes: { notchedOutline: classes.noBorder }
                                }}
                                select>
                                <MenuItem value="Value A">Value A</MenuItem>
                                <MenuItem value="Value B">Value B</MenuItem>
                                <MenuItem value="Value C">Value C</MenuItem>
                            </TextField>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <label className="basicLabel">Kode Pos</label>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                label={kodePosSaatIni === "" ? "Pilih Menu" : ""}
                                InputLabelProps={{ shrink: false }}
                                placeholder="Pilih Kode Pos"
                                onChange={(e) => setKodePosSaatIni(e.target.value)}
                                defaultValue={kodePosSaatIni}
                                // required 
                                fullWidth
                                InputProps={{
                                    classes: { notchedOutline: classes.noBorder }
                                }}
                                select>
                                <MenuItem value="Value A">Value A</MenuItem>
                                <MenuItem value="Value B">Value B</MenuItem>
                                <MenuItem value="Value C">Value C</MenuItem>
                            </TextField>
                        </Grid>
                    </Grid>
                    <label className="basicLabel">No.Telepon Rumah (Opsional)</label>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        label={noTlpRumah === "" ? "Masukkan No.Telepon Rumah" : ""}
                        InputLabelProps={{ shrink: false }}
                        type="number"
                        onChange={(e) => setNoTlpRumah(e.target.value)}
                        defaultValue={noTlpRumah}
                        // required
                        fullWidth
                        InputProps={{
                            classes: { notchedOutline: classes.noBorder }
                        }}
                    />
                    <label className="basicLabel">Alamat Surat Menyurat</label>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        label={alamatSurat === "" ? "Pilih Alamat Surat Menyurat" : ""}
                        InputLabelProps={{ shrink: false }}
                        onChange={(e) => setAlamatSurat(e.target.value)}
                        defaultValue={alamatSurat}
                        // required 
                        fullWidth
                        InputProps={{
                            classes: { notchedOutline: classes.noBorder }
                        }}
                        select>
                        <MenuItem value="Value A">Value A</MenuItem>
                        <MenuItem value="Value B">Value B</MenuItem>
                        <MenuItem value="Value C">Value C</MenuItem>
                    </TextField>
                    <div className="firstPageButtonsWrapper">
                        <div className="">
                            {/* <input
                                className="transparentButton"
                                type="submit"
                                value="Simpan Formulir"
                            /> */}
                        </div>
                        <div className="sliceForSecondPageButton">
                            <input
                                className="secondaryButton"
                                type="submit"
                                value="Kembali"
                                onClick={() => setStepDataDiri(1)}
                            />
                            <input
                                className="primaryButton"
                                type="submit"
                                value="Lanjut"
                                onClick={() => setStepDataDiri(2.1)}
                            />
                        </div>
                    </div>
                </FormControl>
            </Box>
        </Box>
    )
}

export default FormDataDiriPemohonBagian2