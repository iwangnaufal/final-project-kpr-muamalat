/* eslint-disable default-case */
import React, { useContext } from "react";
import DataFasilitasPembiayaan from "../Components/DataFasilitasPembiayaan";
import DataKerabat from "../Components/DataKerabat";
import DataPemohon from "../Components/DataPemohon"
import DataPemohon2 from "../Components/DataPemohon2";
import DataAgunan1 from "../Components/DataAgunan";
import DataAgunan2 from "../Components/DataAgunan2";
import DataPekerjaanPemohon from "../Components/DataPekerjaanPemohon";
import DataPekerjaanPemohon2 from "../Components/DataPekerjaanPemohon2";
import DataDiriPasangan from "../Components/DataDiriPasangan";
import DataPekerjaanPasangan from "../Components/DataPekerjaanPasangan";
import DataPekerjaanPasangan2 from "../Components/DataPekerjaanPasangan2";
import { multiStepContext } from "../StepContext"
import { Stepper, StepLabel, Step } from "@material-ui/core";
import { StepperButton } from "../Components/Button";
import UploadDokumen from "../Components/UploadDokumen"
import "../Styles/Step.css"
import DataFasilitasPembiayaan2 from "../Components/DataFasilitasPembiayaan2";

export default function UserForm() {
    const { page, setPage } = useContext(multiStepContext);

    // let token = localStorage.getItem("token");

    // if (token) {
    //     token = true;
    // } else {
    //     token = false;
    // }

    function showPage(page) {
        switch (page) {
            case 1:
                return <PagePengajuanDiri />
            case 2:
                return <FormUploadDoc />
        }
    }




    const PagePengajuanDiri = () => {
        const { stepDataDiri } = useContext(multiStepContext);

        function showStep(step) {
            switch (step) {
                case 1:
                    window.scrollTo({ behavior: "smooth", top: 20 })
                    return <DataPemohon />;
                case 1.2:
                    window.scrollTo({ behavior: "smooth", top: 20 })
                    return <DataPemohon2 />;
                case 2.1:
                    window.scrollTo({ behavior: "smooth", top: 20 })
                    return <DataFasilitasPembiayaan />;
                case 2.2:
                    window.scrollTo({ behavior: "smooth", top: 20 })
                    return <DataFasilitasPembiayaan2 />;
                case 3.1:
                    window.scrollTo({ behavior: "smooth", top: 20 })
                    return <DataAgunan1 />;
                case 3.2:
                    window.scrollTo({ behavior: "smooth", top: 20 })
                    return <DataAgunan2 />;
                case 4.1:
                    window.scrollTo({ behavior: "smooth", top: 20 })
                    return <DataDiriPasangan />;
                case 4.2:
                    window.scrollTo({ behavior: "smooth", top: 20 })
                    return <DataKerabat />;
                case 5.1:
                    window.scrollTo({ behavior: "smooth", top: 20 })
                    return <DataPekerjaanPemohon />;
                case 5.2:
                    window.scrollTo({ behavior: "smooth", top: 20 })
                    return <DataPekerjaanPemohon2 />;
                case 5.3:
                    window.scrollTo({ behavior: "smooth", top: 20 })
                    return <DataPekerjaanPasangan />;
                case 5.4:
                    window.scrollTo({ behavior: "smooth", top: 20 })
                    return <DataPekerjaanPasangan2 />;
            }
        }

        return (
            <>
                <Stepper
                    style={{ width: "60vw", background: "#FBFBFB" }}
                    activeStep={stepDataDiri - 1}
                    orientation="horizontal"
                    alternativeLabel
                >
                    <Step>
                        <StepLabel>Data Pemohon</StepLabel>
                    </Step>
                    <Step>
                        <StepLabel>Fasilitas Pembayaran</StepLabel>
                    </Step>
                    <Step>
                        <StepLabel>Data Agunan</StepLabel>
                    </Step>
                    <Step>
                        <StepLabel>Data Keluarga</StepLabel>
                    </Step>
                    <Step>
                        <StepLabel>Data Pekerjaan</StepLabel>
                    </Step>
                </Stepper>
                {showStep(stepDataDiri)}
            </>
        )
    };

    const FormUploadDoc = () => {
        return <UploadDokumen />;
    };

    return (
        <div className="App">
            {/* <Heading/> */}
            <div className="App-header">
                <div style={{ width: "60%", textAlign: "left" }}>
                    <h3 id="h3Title">Pengajuan KPR Bank Muamalat</h3>
                </div>
                <div
                    id="boxStepperWrapper"
                    style={{
                        display: page === 4 ? "none" : "flex",
                    }}
                >
                    <StepperButton
                        title="Pengisian Data"
                        idPage={1}
                        currentPage={page}
                        onClick={() => setPage(1)}
                    />
                    <StepperButton
                        title="Upload Dokumen"
                        idPage={2}
                        currentPage={page}
                        onClick={() => setPage(2)}
                    />
                    <StepperButton
                        title="RIngkasan"
                        idPage={3}
                        currentPage={page}
                        onClick={() => setPage(3)}
                    />
                </div>
                {showPage(page)}
            </div>
            {/* <Footer/> */}
        </div>
    )
}