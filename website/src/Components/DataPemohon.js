import React, { useState, useContext } from 'react';
import { styled } from '@mui/material/styles';
import { Box, Grid, Radio, RadioGroup, FormControlLabel, FormControl, Divider } from '@mui/material'
import '../Styles/styles.css'
import { makeStyles } from "@material-ui/core/styles";
import { TextField, MenuItem } from '@material-ui/core';
import { multiStepContext } from "../StepContext"

const useStyles = makeStyles(() => ({
  noBorder: {
    border: "none",
  }
}));

const BpIcon = styled("span")(({ theme }) => ({
  borderRadius: "50%",
  width: "7px",
  height: "7px",
  border: "1.5px solid #bfbfbf",
  padding: "3px",
  "input:hover ~ &": {
    backgroundColor: "#ebebeb"
  },
  "input:disabled ~ &": {
    boxShadow: "none",
    background:
      theme.palette.mode === "dark"
        ? "rgba(57,75,89,.5)"
        : "rgba(206,217,224,.5)"
  }
}));

const BpCheckedIcon = styled(BpIcon)({
  top: "3.5px",
  left: "3.5px",
  backgroundImage:
    "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
  "&:before": {
    display: "block",
    width: "7.5px",
    height: "7.5px",
    backgroundImage: "radial-gradient(#500878,#500878,#500878)",
    borderRadius: "50%",
    content: '""'
  },
  "input:hover ~ &": {
    // backgroundColor: '#106ba3',
  }
});

// Inspired by blueprintjs
function BpRadio(props) {
  return (
    <Radio
      sx={{
        "&:hover": {
          bgcolor: "transparent"
        }
      }}
      disableRipple
      color="default"
      checkedIcon={<BpCheckedIcon />}
      icon={<BpIcon />}
      {...props}
    />
  );
}

export default function DataPemohon() {
  const { setStepDataDiri, userData, setUserData } = useContext(multiStepContext);
  const [nik, setNik] = useState("");
  const [namaError, setNamaError] = useState("");
  const [emailAddressError, setEmailAddressError] = useState("");
  const [nama, setNama] = useState("");
  const [tempatLahir, setTempatLahir] = useState("");
  const [tanggalLahir, setTanggalLahir] = useState("");
  const [jenisKelamin, setJenisKelamin] = useState("");
  const [statusPerkawinan, setStatusPerkawinan] = useState("");
  const [pendidikanTerakhir, setPendidikanTerakhir] = useState("");
  const [alamatEmail, setAlamatEmail] = useState("");
  const [noHp, setNoHp] = useState("");
  const [noHpOptinal, setNoHpOptinal] = useState("");
  const [noNpwp, setnoNpwp] = useState("");
  const [namaIbu, setNamaIbu] = useState("");

  const classes = useStyles();


  return (
    <Box sx={{
      width: '80%',
      bgcolor: '#fff',
      alignItems: 'center',
      paddingTop: '48px',
      paddingBottom: '48px',
      borderRadius: '34px'

    }}>
      <Box sx={{
        width: '70%',
        marginLeft: '15%'
      }}>
        <h2 className="titleOne">Data Pemohon</h2>
        <Divider
          sx={{
            marginTop: '24px',
            border: '0, 1px solid #363636',
            backgroundColor: '#363636'
          }}
        />
        <FormControl component="fieldset" sx={{ width: '100%' }}>
          <label className="basicLabel">NIK e-KTP</label>
          <TextField
            variant="outlined"
            margin="normal"
            label={nik === "" ? "Masukkan NIK e-KTP" : ""}
            InputLabelProps={{ shrink: false }}
            onChange={(e) => setNik(e.target.value)}
            defaultValue={nik}
            fullWidth
            // helperText="0/16"
            InputProps={{
              classes: { notchedOutline: classes.noBorder }
            }}
          />
          <label className="basicLabel">Nama Lengkap Sesuai KTP</label>
          <TextField
            variant="outlined"
            margin="normal"
            label={nama === "" ? "Masukkan Nama Lengkap" : ""}
            InputLabelProps={{ shrink: false }}
            onChange={(e) => setNama(e.target.value)}
            defaultValue={nama}
            fullWidth
            InputProps={{
              classes: { notchedOutline: classes.noBorder }
            }}
          />
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <label className="basicLabel">Tempat Lahir</label>
              <TextField
                variant="outlined"
                margin="normal"
                label={tempatLahir === "" ? "Masukkan Tempat Lahir" : ""}
                InputLabelProps={{ shrink: false }}
                onChange={(e) => setTempatLahir(e.target.value)}
                defaultValue={tempatLahir}
                // required
                fullWidth
                InputProps={{
                  classes: { notchedOutline: classes.noBorder }
                }}
              />
            </Grid>

            <Grid item xs={12} sm={6}>
              <label className="basicLabel">Tanggal Lahir</label>
              <TextField
                variant="outlined"
                margin="normal"
                // label={tanggalLahir === "" ? "Masukkan Label Grid 1" : ""}
                InputLabelProps={{ shrink: false }}
                type="date"
                onChange={(e) => setTanggalLahir(e.target.value)}
                defaultValue={tanggalLahir}
                // required
                fullWidth
                InputProps={{
                  classes: { notchedOutline: classes.noBorder }
                }}
              />
            </Grid>
          </Grid>
            <label className="basicLabel">Jenis Kelamin</label>
            <RadioGroup
              aria-label="gender"
              name="controlled-radio-buttons-group"
              sx={{
                width: '100%'
              }}
            >
              <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                  <FormControlLabel value="Laki-laki" control={<BpRadio />}
                    label={<span style={{ fontFamily: 'Poppins', fontSize: '14px' }}>{"Laki-laki"}</span>}
                    sx={{
                      backgroundColor: '#f4f4f4',
                      borderRadius: '8px',
                      height: '48px',
                      marginLeft: '0px',
                      paddingLeft: '0px',
                      width: '100%',
                      color: '#363636',
                    }}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <FormControlLabel value="Perempuan" control={<BpRadio />}
                    label={<span style={{ fontFamily: 'Poppins', fontSize: '14px' }}>{"Perempuan"}</span>}
                    sx={{
                      backgroundColor: '#f4f4f4',
                      borderRadius: '8px',
                      height: '48px',
                      marginLeft: '0px',
                      paddingLeft: '0px',
                      width: '100%',
                      color: '#363636'
                    }}
                  />
                </Grid>
              </Grid>
            </RadioGroup>
            <label className="basicLabel">Status Perkawinan</label>
            <TextField
              variant="outlined"
              margin="normal"
              label={statusPerkawinan === "" ? "Pilih Status Perkawinan" : ""}
              InputLabelProps={{ shrink: false }}
              onChange={(e) => setStatusPerkawinan(e.target.value)}
              defaultValue={statusPerkawinan}
              // required 
              fullWidth
              InputProps={{
                classes: { notchedOutline: classes.noBorder }
              }}
              select>
              <MenuItem value="Lajang">Lajang</MenuItem>
              <MenuItem value="Menikah">Menikah</MenuItem>
              <MenuItem value="PisahHarta">Menikah (Pisah Harta)</MenuItem>
              <MenuItem value="CeraiHidup">Cerai Hidup</MenuItem>
              <MenuItem value="CeraiMati">Cerai Mati</MenuItem>

            </TextField>
            <label className="basicLabel">Pendidikan Terakhir</label>
            <TextField
              variant="outlined"
              margin="normal"
              label={pendidikanTerakhir === "" ? "Pilih Pendidikan Terakhir" : ""}
              InputLabelProps={{ shrink: false }}
              onChange={(e) => setPendidikanTerakhir(e.target.value)}
              defaultValue={pendidikanTerakhir}
              // required 
              fullWidth
              InputProps={{
                classes: { notchedOutline: classes.noBorder }
              }}
              select>
              <MenuItem value="Value A">S1</MenuItem>
              <MenuItem value="Value B">Value B</MenuItem>
              <MenuItem value="Value C">Value C</MenuItem>
            </TextField>
          <label className="basicLabel">Alamat Email</label>
          <TextField
            variant="outlined"
            margin="normal"
            label={alamatEmail === "" ? "Masukkan Alamat Email" : ""}
            InputLabelProps={{ shrink: false }}
            type="email" name="alamatEmail"
            onChange={(e) => setAlamatEmail(e.target.value)}
            defaultValue={alamatEmail}
            fullWidth
            InputProps={{
              classes: { notchedOutline: classes.noBorder }
            }}
          />
          <label className="basicLabel">No. Handphone </label>
          <TextField
            variant="outlined"
            margin="normal"
            label={noHp === "" ? "81234567890" : ""}
            InputLabelProps={{ shrink: false }}
            type="number" min="1" 
            onChange={(e) => setNoHp(e.target.value)}
            defaultValue={noHp}
            // required
            fullWidth
            // helperText="Nomor Handphone Minimal 10 Digit"
            InputProps={{
              classes: { notchedOutline: classes.noBorder }
            }}
          />
            <label className="basicLabel">No. Handphone (Optional)</label>
            <TextField
              variant="outlined"
              margin="normal"
              label={noHpOptinal === "" ? "81234567890" : ""}
              InputLabelProps={{ shrink: false }}
              type="number" min="1"
              onChange={(e) => setNoHpOptinal(e.target.value)}
              defaultValue={noHpOptinal}
              // required
              fullWidth
              // helperText="Nomor Handphone Minimal 10 Digit"
              InputProps={{
                classes: { notchedOutline: classes.noBorder }
              }}
            />
            <label className="basicLabel">Nomor NPWP</label>
            <TextField
              variant="outlined"
              margin="normal"
              label={noNpwp === "" ? "Masukkan Nomor NPWP" : ""}
              InputLabelProps={{ shrink: false }}
              type="number"
              onChange={(e) => setnoNpwp(e.target.value)}
              defaultValue={noNpwp}
              // required
              fullWidth
              // helperText="0/15"
              InputProps={{
                classes: { notchedOutline: classes.noBorder }
              }}
            />
            <label className="basicLabel">Nama Gadis Ibu Kandung</label>
            <TextField
              variant="outlined"
              margin="normal"
              label={namaIbu === "" ? "Masukkan Nama Gadis Ibu Kandung" : ""}
              InputLabelProps={{ shrink: false }}
              onChange={(e) => setNamaIbu(e.target.value)}
              defaultValue={namaIbu}
              // required
              fullWidth
              InputProps={{
                classes: { notchedOutline: classes.noBorder }
              }}
            />
          <div className="firstPageButtonsWrapper">
            <div className="">
              {/* <input
                className="transparentButton"
                type="submit"
                value="Simpan Formulir"
              /> */}
            </div>
            <div className="sliceForSecondPageButton">
              <input
                className="secondaryButton"
                type="submit"
                value="Kembali"
              // onClick={() => setStepDataDiri(3.1)}
              />
              <input
                className="primaryButton"
                type="submit"
                value="Lanjut"
                onClick={() => setStepDataDiri(1.2)}
              />
            </div>
          </div>
        </FormControl>
      </Box>
    </Box>
  );
}
