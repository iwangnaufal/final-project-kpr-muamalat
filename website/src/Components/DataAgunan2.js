import React, { useState, useContext } from "react";
import { styled } from '@mui/material/styles';
import { Box, Grid, Radio, RadioGroup, FormControlLabel, FormControl, Divider } from '@mui/material'
import '../Styles/styles.css'
import { makeStyles } from "@material-ui/core/styles";
import { TextField, MenuItem } from '@material-ui/core';
import InputUnstyled from "@mui/core/InputUnstyled";
import { multiStepContext } from "../StepContext"

const StyledInputElement = styled("input")(`
  width: calc(100% - 16px);
  font-family: Poppins;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 24px;
  background: rgb(243, 246, 249);
  border: 0px solid #E5E8EC;
  border-radius: 8px;
  // padding: 6px 10px;
  color: #20262D;
  height: 48px;
  background: #f4f4f4;
  padding-left: 16px;

  &:hover {
    background: #ede5ee;
  }

  &:focus {
    outline: none;
    background: #ede5ee;
  }
`);

const StyledInputWithIconWrapper = styled("div")(`
  position: relative;
  input {
    display: flex;
    flex-direction: row;
    padding-left: 56px;
  }

  label {
    position: absolute;
    left: 0px;
    top: 0px;
    background-color: #e3e3e3;
    height: 36px;
    width: 48px;
    border-radius: 8px 0px 0px 8px;
    text-align: center;
    padding-top: 12px;
    color: #9E9E9E;
  }
  input &:focus + label {
    background-color: #dccee4;
  }
`)

const StyledInputWithIcon = styled("input")(`
  width: calc(100% - 56px);
  font-family: Poppins;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 24px;
  border: 0px solid #E5E8EC;
  border-radius: 8px;
  // padding: 6px 10px;
  color: #20262D;
  height: 48px;
  background: #f4f4f4;
  padding-left: 16px;

  &:hover {
    background: #ede5ee;
  }

  &:focus {
    outline: none;
    background: #ede5ee;
  }
`);

const CustomInput = React.forwardRef(function CustomInput(props, ref) {
  return (
    <InputUnstyled
      components={{ Input: StyledInputElement }}
      {...props}
      ref={ref}
      select
    />
  );
});

const CustomInputWithIcon = React.forwardRef(function CustomInput(props, ref) {
  return (
    <InputUnstyled
      components={{ Input: StyledInputWithIcon }}
      {...props}
      ref={ref}
      select
    />
  );
});

const useStyles = makeStyles(() => ({
  noBorder: {
    border: "none",
  }
}));

const BpIcon = styled("span")(({ theme }) => ({
  borderRadius: "50%",
  width: "7px",
  height: "7px",
  border: "1.5px solid #bfbfbf",
  padding: "3px",
  "input:hover ~ &": {
    backgroundColor: "#ebebeb"
  },
  "input:disabled ~ &": {
    boxShadow: "none",
    background:
      theme.palette.mode === "dark"
        ? "rgba(57,75,89,.5)"
        : "rgba(206,217,224,.5)"
  }
}));

const BpCheckedIcon = styled(BpIcon)({
  top: "3.5px",
  left: "3.5px",
  backgroundImage:
    "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
  "&:before": {
    display: "block",
    width: "7.5px",
    height: "7.5px",
    backgroundImage: "radial-gradient(#500878,#500878,#500878)",
    borderRadius: "50%",
    content: '""'
  },
  "input:hover ~ &": {
    // backgroundColor: '#106ba3',
  }
});

// Inspired by blueprintjs
function BpRadio(props) {
  return (
    <Radio
      sx={{
        "&:hover": {
          bgcolor: "transparent"
        }
      }}
      disableRipple
      color="default"
      checkedIcon={<BpCheckedIcon />}
      icon={<BpIcon />}
      {...props}
    />
  );
}

export default function DataAgunan2() {
  const classes = useStyles();
  const [value, setValue] = React.useState('female');
  const { setStepDataDiri, userData, setUserData } = useContext(multiStepContext);

  const [peruntukan_pembiayaan, setPeruntukan_Pembiayaan] = useState("")
  const [program, setProgram] = useState("")
  const [status_agunan, setStatus_Agunan] = useState("")
  const [status_kepemilikan, setStatus_Kepemilikan] = useState("")
  const [jenis_agunan, setJenis_Agunan] = useState("")
  const handleChange = (event) => {
    setValue(event.target.value);
  };

  return (
    <Box sx={{
      width: '80%',
      bgcolor: '#fff',
      alignItems: 'center',
      paddingTop: '48px',
      paddingBottom: '48px',
      borderRadius: '34px'

    }}>
      <Box sx={{
        width: '70%',
        marginLeft: '15%'
      }}>
        <h2 className="titleOne">Data Agunan</h2>
        <Divider
          sx={{
            marginTop: '24px',
            border: '0, 1px solid #363636',
            backgroundColor: '#363636'
          }}
        />
        {/* <h3 className="titleTwo">Fasilitas Pembiayaan</h3> */}

        <FormControl component="fieldset" sx={{ width: '100%' }}>

          <label className="basicLabel">Nomor SPR Developer</label>
          <CustomInput aria-label="Nomor SPR Developer" placeholder="Masukkan Nomor SPR Developer" />

          <label className="basicLabel">Alamat Agunan</label>
          <CustomInput aria-label="Alamat Agunan" placeholder="Masukkan Alamat Agunan" />

          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <Grid container spacing={1}>
                <Grid item xs={12} sm={6}>
                  <label className="basicLabel">RT</label>
                  <CustomInput aria-label="RT" placeholder="000" />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <label className="basicLabel">RW</label>
                  <CustomInput aria-label="RW" placeholder="000" />
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} sm={6}>
              <label className="basicLabel">Provinsi</label>
              <CustomInput aria-label="Provinsi" placeholder="Masukkan Provinsi" />
            </Grid>
          </Grid>

          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <label className="basicLabel">Kabupaten/Kota</label>
              <CustomInput aria-label="Provinsi" placeholder="Masukkan Kabupaten/Kota" />
            </Grid>
            <Grid item xs={12} sm={6}>
              <label className="basicLabel">Kecamatan</label>
              <CustomInput aria-label="Provinsi" placeholder="Masukkan Kecamatan" />
            </Grid>
          </Grid>

          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <label className="basicLabel">Kelurahan</label>
              <CustomInput aria-label="Provinsi" placeholder="Masukkan Kelurahan" />
            </Grid>
            <Grid item xs={12} sm={6}>
              <label className="basicLabel">Kode Pos</label>
              <CustomInput aria-label="Provinsi" placeholder="Masukkan Kode Pos" />
            </Grid>
          </Grid>

          <div className="firstPageButtonsWrapper">
            <div className="">
              {/* <input
                className="transparentButton"
                type="submit"
                value="Simpan Formulir"
                hidden
              /> */}
            </div>
            <div className="sliceForSecondPageButton">
              <input
                className="secondaryButton"
                type="submit"
                value="Kembali"
                onClick={() => setStepDataDiri(3.1)}
              />
              <input
                className="primaryButton"
                type="submit"
                value="Lanjut"
                onClick={() => setStepDataDiri(4.1)}
              />
            </div>
          </div>


        </FormControl>
      </Box>
    </Box>
  )
}
