import React, { useState, useContext } from "react";
import { styled } from '@mui/material/styles';
import { Box, Grid, FormControl, Divider } from '@mui/material'
import '../Styles/styles.css'
import { makeStyles } from "@material-ui/core/styles";
import { TextField, MenuItem } from '@material-ui/core';
import { multiStepContext } from "../StepContext"

const useStyles = makeStyles(() => ({
  noBorder: {
    border: "none",
  }
}));

export default function DataKerabat() {
  const classes = useStyles();
  const { setStepDataDiri, userData, setUserData } = useContext(multiStepContext);
  const [namaKerabat, setNamaKerabat] = useState("")
  const [hubunganDenganNasabah, setHubunganDenganNasabah] = useState("");
  const [telpRumahKerabat, setTelpRumahKerabat] = useState("");
  const [noHpKerabat, setNoHpKerabat] = useState("");

  return (
    <Box sx={{
      width: '80%',
      bgcolor: '#fff',
      alignItems: 'center',
      paddingTop: '48px',
      paddingBottom: '48px',
      borderRadius: '34px'

    }}>
      <Box sx={{
        width: '70%',
        marginLeft: '15%'
      }}>
        <h2 className="titleOne">Data Keluarga</h2>
        <Divider
          sx={{
            marginTop: '24px',
            marginBottom: '24px',
            border: '0, 1px solid #363636',
            backgroundColor: '#363636'
          }}
        />
        <h3 className="titleTwo">Kerabat Yang Tidak Tinggal Serumah</h3>

        <FormControl component="fieldset" sx={{ width: '100%' }}>
          <label className="basicLabel">Nama Kerabat</label>
          <TextField
            variant="outlined"
            margin="normal"
            label={namaKerabat === "" ? "Masukkan Nama Kerabat" : ""}
            InputLabelProps={{ shrink: false }}
            defaultValue={namaKerabat}
            onChange={(e) => setNamaKerabat(e.target.value)}
            // required
            fullWidth
            InputProps={{
              classes: { notchedOutline: classes.noBorder }
            }}
          />
          <label className="basicLabel">Hubungan Dengan Nasabah</label>
          <TextField
            variant="outlined"
            margin="normal"
            label={hubunganDenganNasabah === "" ? "Pilih Hubungan Dengan Nasabah" : ""}
            InputLabelProps={{ shrink: false }}
            defaultValue={hubunganDenganNasabah}
            onChange={(e) => setHubunganDenganNasabah(e.target.value)}
            // required
            fullWidth
            InputProps={{
              classes: { notchedOutline: classes.noBorder }
            }}
            select>
            <MenuItem value="Orang Tua">Orang Tua</MenuItem>
            <MenuItem value="Saudara Kandung">Saudara Kandung</MenuItem>
            <MenuItem value="Anak Kandung">Anak Kandung</MenuItem>
            <MenuItem value="Paman / Bibi">Paman / Bibi</MenuItem>
            <MenuItem value="Kakek / Nenek">Kakek / Nenek</MenuItem>
            <MenuItem value="Ipar">Ipar</MenuItem>
            <MenuItem value="Mertua">Mertua</MenuItem>
            <MenuItem value="Menantu">Menantu</MenuItem>
            <MenuItem value="Keponakan">Keponakan</MenuItem>
            <MenuItem value="Sepupu">Sepupu</MenuItem>
          </TextField>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <label className="basicLabel">No. Telepon Rumah (Optional) </label>
              <TextField
                variant="outlined"
                margin="normal"
                label={telpRumahKerabat === "" ? "Masukkan Nomor Telepon" : ""}
                InputLabelProps={{ shrink: false }}
                defaultValue={telpRumahKerabat}
                onChange={(e) => setTelpRumahKerabat(e.target.value)}
                // required
                fullWidth
                InputProps={{
                  classes: { notchedOutline: classes.noBorder }
                }}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <label className="basicLabel">No. Handphone</label>
              <div className="inputWithIconLeftWrapper">
                <input
                  className="inputWithIconLeft"
                  type="number"
                  placeholder="81234567890"
                  onChange={(e) => setNoHpKerabat(e.target.value)}
                  defaultValue={noHpKerabat}
                />
                <label className="iconLeft">+62</label>
              </div>
            </Grid>
          </Grid>

          <div className="firstPageButtonsWrapper">
            <div className="">
              {/* <input
                className="transparentButton"
                type="submit"
                value="Simpan Formulir"
              /> */}
            </div>
            <div className="sliceForSecondPageButton">
              <input
                className="secondaryButton"
                type="submit"
                value="Kembali"
                onClick={() => setStepDataDiri(4.1)}
              />
              <input
                className="primaryButton"
                type="submit"
                value="Lanjut"
                onClick={() => setStepDataDiri(5.1)}
              />
            </div>
          </div>
        </FormControl>
      </Box>
    </Box>
  )
}
