import React, { useState, useContext } from "react";
import { styled } from '@mui/material/styles';
import { Box, Grid, Radio, RadioGroup, FormControlLabel, FormControl, Divider } from '@mui/material'
import '../Styles/styles.css'
import { makeStyles } from "@material-ui/core/styles";
import { TextField, MenuItem } from '@material-ui/core';
import { multiStepContext } from "../StepContext"

const useStyles = makeStyles(() => ({
  noBorder: {
    border: "none",
  }
}));

const BpIcon = styled("span")(({ theme }) => ({
  borderRadius: "50%",
  width: "7px",
  height: "7px",
  border: "1.5px solid #bfbfbf",
  padding: "3px",
  "input:hover ~ &": {
    backgroundColor: "#ebebeb"
  },
  "input:disabled ~ &": {
    boxShadow: "none",
    background:
      theme.palette.mode === "dark"
        ? "rgba(57,75,89,.5)"
        : "rgba(206,217,224,.5)"
  }
}));

const BpCheckedIcon = styled(BpIcon)({
  top: "3.5px",
  left: "3.5px",
  backgroundImage:
    "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
  "&:before": {
    display: "block",
    width: "7.5px",
    height: "7.5px",
    backgroundImage: "radial-gradient(#500878,#500878,#500878)",
    borderRadius: "50%",
    content: '""'
  },
  "input:hover ~ &": {
    // backgroundColor: '#106ba3',
  }
});

// Inspired by blueprintjs
function BpRadio(props) {
  return (
    <Radio
      sx={{
        "&:hover": {
          bgcolor: "transparent"
        }
      }}
      disableRipple
      color="default"
      checkedIcon={<BpCheckedIcon />}
      icon={<BpIcon />}
      {...props}
    />
  );
}

export default function DataFasilitasPembiayaan() {
  const classes = useStyles();
  const { setStepDataDiri, userData, setUserData } = useContext(multiStepContext);

  const [peruntukan_pembiayaan, setPeruntukan_Pembiayaan] = useState("")
  const [program, setProgram] = useState("")
  const [programLainnya, setProgramLainnya] = useState("")
  const [akadFasilitasPembiayaan, setAkad] = useState("")
  const [totalPlafondDiajukaPembiayaan, setPlafond] = useState("")
  const [jenisPenjualPembiayaan, setJenisPenjual] = useState("")
  const [namaPenjualPembiayaan, setNamaPenjual] = useState("")
  const [hargaPenawaranSPRPembiayaan, setHargaPenawaran] = useState("")
  const [nomorTeleponPenjual, setNomorTeleponPenjual] = useState("")

  const handleAkad = (e) => {
    console.log(e.target.value);
    if (e.target.value === 10) {
      setProgram(true)
    } else {
      setProgram(false)
    }
  };

  return (
    <Box sx={{
      width: '80%',
      bgcolor: '#fff',
      alignItems: 'center',
      paddingTop: '48px',
      paddingBottom: '48px',
      borderRadius: '34px'

    }}>
      <Box sx={{
        width: '70%',
        marginLeft: '15%'
      }}>
        <h2 className="titleOne">Fasilitas Pembiayaan</h2>
        <Divider
          sx={{
            marginTop: '24px',
            border: '0, 1px solid #363636',
            backgroundColor: '#363636'
          }}
        />

        <FormControl component="fieldset" sx={{ width: '100%' }}>
          <label className="basicLabel">Skema Pengajuan</label>
          <RadioGroup
            aria-label="gender"
            name="controlled-radio-buttons-group"
            sx={{
              width: '100%'
            }}
          >
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <FormControlLabel value="Penghasilan Tunggal" control={<BpRadio />}
                  label={<span style={{ fontFamily: 'Poppins', fontSize: '14px' }}>{"Penghasilan Tunggal"}</span>}
                  sx={{
                    backgroundColor: '#f4f4f4',
                    borderRadius: '8px',
                    height: '48px',
                    marginLeft: '0px',
                    paddingLeft: '0px',
                    width: '100%',
                    color: '#363636',
                  }}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <FormControlLabel value="Penghasilan Gabungan" control={<BpRadio />}
                  label={<span style={{ fontFamily: 'Poppins', fontSize: '14px' }}>{"Penghasilan Gabungan"}</span>}
                  sx={{
                    backgroundColor: '#f4f4f4',
                    borderRadius: '8px',
                    height: '48px',
                    marginLeft: '0px',
                    paddingLeft: '0px',
                    width: '100%',
                    color: '#363636'
                  }}
                />
              </Grid>
            </Grid>
          </RadioGroup>
          <label className="basicLabel">Peruntukkan Pembiayaan</label>
          <TextField
            variant="outlined"
            margin="normal"
            label={peruntukan_pembiayaan === "" ? "Pilih Objek yang Dibiayai" : ""}
            InputLabelProps={{ shrink: false }}
            defaultValue={peruntukan_pembiayaan}
            onChange={(e) => setPeruntukan_Pembiayaan(e.target.value)}
            // required
            fullWidth
            InputProps={{
              classes: { notchedOutline: classes.noBorder }
            }}
            select>
            <MenuItem value="Properti">Pembelian Properti</MenuItem>
          </TextField>
          <label className="basicLabel">Program</label>
          <TextField
            variant="outlined"
            margin="normal"
            label={program === "" ? "Pilih Program" : ""}
            InputLabelProps={{ shrink: false }}
            defaultValue={program}
            onChange={(e) => setProgram(e.target.value)}
            fullWidth
            className={classes.textField}
            placeholder="Pilih Program"
            InputProps={{
              classes: { notchedOutline: classes.noBorder }
            }}
            select>
            <MenuItem value="Fix & Fix">Fix & Fix</MenuItem>
            <MenuItem value="Angsuran Super Ringan">Angsuran Super Ringan</MenuItem>
            <MenuItem value="Special MMQ">Special MMQ</MenuItem>
            <MenuItem value="Lainnya">Lainnya</MenuItem>

          </TextField>
          {
            program === "Lainnya" ?

              <TextField
                label={programLainnya === "" ? "Masukkan Program Lainnya" : ""}
                InputLabelProps={{ shrink: false }}
                defaultValue={programLainnya}
                margin="normal"
                variant="outlined"
                InputProps={{
                  classes: { notchedOutline: classes.noBorder }
                }}
                onChange={(e) => setProgramLainnya(e.target.value)}
                fullWidth />
              : null
          }

          <label className="basicLabel">Akad Fasilitas Yang Diajukan</label>
          <TextField
            variant="outlined"
            margin="normal"
            label={akadFasilitasPembiayaan === "" ? "Pilih Akad Fasilitas Yang Diajukan" : ""}
            InputLabelProps={{ shrink: false }}
            defaultValue={akadFasilitasPembiayaan}
            onChange={(e) => setAkad(e.target.value)}
            fullWidth
            className={classes.textField}
            placeholder="Pilih Akad Fasilitas Yang Diajukan"
            InputProps={{
              classes: { notchedOutline: classes.noBorder }
            }}
            select>
            <MenuItem value="Fix & Fix">Murabahah</MenuItem>
            <MenuItem value="Angsuran Super Ringan">MMQ (Musyawarah Mustanaqishah</MenuItem>
            <MenuItem value="Special MMQ">Istishna</MenuItem>
            <MenuItem value="Lainnya">Lainnya</MenuItem>
          </TextField>

          <label className="basicLabel">Total Plafond Yang Diajukan</label>
          <TextField
            variant="outlined"
            margin="normal"
            label={totalPlafondDiajukaPembiayaan === "" ? "0" : ""}
            InputLabelProps={{ shrink: false }}
            defaultValue={totalPlafondDiajukaPembiayaan}
            onChange={(e) => setPlafond(e.target.value)}
            // required
            fullWidth
            InputProps={{
              classes: { notchedOutline: classes.noBorder }
            }}
          />

          <label className="basicLabel">Jenis Penjual</label>
          <TextField
            variant="outlined"
            margin="normal"
            label={jenisPenjualPembiayaan === "" ? "Pilih Jenis Penjual" : ""}
            InputLabelProps={{ shrink: false }}
            defaultValue={jenisPenjualPembiayaan}
            onChange={(e) => setJenisPenjual(e.target.value)}
            fullWidth
            className={classes.textField}
            // placeholder="Pilih Program"
            InputProps={{
              classes: { notchedOutline: classes.noBorder }
            }}
            select>
            <MenuItem value="Fix & Fix">Developer Rekanan</MenuItem>
            <MenuItem value="Angsuran Super Ringan">Developer Non Rekanan</MenuItem>
            <MenuItem value="Special MMQ">Non Developer</MenuItem>
          </TextField>

          <label className="basicLabel">Nama Penjual</label>
          <TextField
            variant="outlined"
            margin="normal"
            label={namaPenjualPembiayaan === "" ? "Masukan Nama Penjual" : ""}
            InputLabelProps={{ shrink: false }}
            defaultValue={namaPenjualPembiayaan}
            onChange={(e) => setNamaPenjual(e.target.value)}
            // required
            fullWidth
            InputProps={{
              classes: { notchedOutline: classes.noBorder }
            }}
          />

          <label className="basicLabel">Harga Penawaran Penjual Atau Nilai SPR</label>
          <TextField
            variant="outlined"
            margin="normal"
            label={hargaPenawaranSPRPembiayaan === "" ? "0" : ""}
            InputLabelProps={{ shrink: false }}
            defaultValue={hargaPenawaranSPRPembiayaan}
            onChange={(e) => setHargaPenawaran(e.target.value)}
            // required
            fullWidth
            InputProps={{
              classes: { notchedOutline: classes.noBorder }
            }}
            // helperText=" Surat Pemesanan Rumah"
          />

          <label className="basicLabel">Nomor Telepon Penjual</label>
          <TextField
            variant="outlined"
            margin="normal"
            label={nomorTeleponPenjual === "" ? "Masukkan No. Telepon Penjual" : ""}
            InputLabelProps={{ shrink: false }}
            defaultValue={nomorTeleponPenjual}
            onChange={(e) => setNomorTeleponPenjual(e.target.value)}
            // required
            fullWidth
            InputProps={{
              classes: { notchedOutline: classes.noBorder }
            }}
          />
          <div className="firstPageButtonsWrapper">
            <div className="">
              {/* <input
                className="transparentButton"
                type="submit"
                value="Simpan Formulir"
              /> */}
            </div>
            <div className="sliceForSecondPageButton">
              <input
                className="secondaryButton"
                type="submit"
                value="Kembali"
                onClick={() => setStepDataDiri(1.2)}
              />
              <input
                className="primaryButton"
                type="submit"
                value="Lanjut"
                onClick={() => setStepDataDiri(2.2)}
              />
            </div>
          </div>
        </FormControl>
      </Box>
    </Box>
  )
}
