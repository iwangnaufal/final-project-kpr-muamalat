import React, { useState, useContext } from "react";
import { Box, Grid, FormControl, Divider } from '@mui/material'
import '../Styles/styles.css'
import { makeStyles, styled } from "@material-ui/core/styles";
import { TextField } from '@material-ui/core';
import { multiStepContext } from "../StepContext"

const useStyles = makeStyles(() => ({
  noBorder: {
    border: "none",
  }
}));

export default function DataDiriPasangan() {
  const classes = useStyles();
  const { setStepDataDiri, userData, setUserData } = useContext(multiStepContext);

  const [namaPasangan, setNamaPasangan] = useState("");
  const [tempatLahirPasangan, setTempatLahirPasangan] = useState("");
  const [tanggalLahirPasangan, setTanggalLahirPasangan] = useState("");
  const [nomorKTPPasangan, setNomorKTPPasangan] = useState("");
  const [nomorNPWPPasangan, setNomorNPWPPasangan] = useState("");
  const [nomorTeleponPasangan, setNomorTeleponPasangan] = useState("");
  const [emailPasangan, setEmailPasangan] = useState("");

  return (
    <Box sx={{
      width: '80%',
      bgcolor: '#fff',
      alignItems: 'center',
      paddingTop: '48px',
      paddingBottom: '48px',
      borderRadius: '34px'

    }}>
      <Box sx={{
        width: '70%',
        marginLeft: '15%'
      }}>
        <h2 className="titleOne">Data Keluarga</h2>
        <Divider
          sx={{
            marginTop: '24px',
            border: '0, 1px solid #363636',
            backgroundColor: '#363636'
          }}
        />
        <h3 className="titleTwo">Data Diri Pasangan</h3>
        <FormControl component="fieldset" sx={{ width: '100%' }}>
          <label className="basicLabel">NIK e-KTP Pasangan</label>
          <TextField
            variant="outlined"
            margin="normal"
            label={nomorKTPPasangan === "" ? "Masukkan NIK" : ""}
            InputLabelProps={{ shrink: false }}
            defaultValue={nomorKTPPasangan}
            onChange={(e) => setNomorKTPPasangan(e.target.value)}
            // required
            fullWidth
            InputProps={{
              classes: { notchedOutline: classes.noBorder }
            }}
          />
          <label className="basicLabel">Nama Pasangan</label>
          <TextField
            variant="outlined"
            margin="normal"
            label={namaPasangan === "" ? "Masukkan Nama Pasangan" : ""}
            InputLabelProps={{ shrink: false }}
            defaultValue={namaPasangan}
            onChange={(e) => setNamaPasangan(e.target.value)}
            // required
            fullWidth
            InputProps={{
              classes: { notchedOutline: classes.noBorder }
            }}
          />
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <label className="basicLabel">Tempat Lahir</label>
              <TextField
                variant="outlined"
                margin="normal"
                label={tempatLahirPasangan === "" ? "Masukkan Tempat Lahir" : ""}
                InputLabelProps={{ shrink: false }}
                defaultValue={tempatLahirPasangan}
                onChange={(e) => setTempatLahirPasangan(e.target.value)}
                // required
                fullWidth
                InputProps={{
                  classes: { notchedOutline: classes.noBorder }
                }}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <label className="basicLabel">Tanggal Lahir</label>
              <TextField
                variant="outlined"
                margin="normal"
                InputLabelProps={{ shrink: false }}
                defaultValue={tanggalLahirPasangan}
                onChange={(e) => setTanggalLahirPasangan(e.target.value)}
                // required
                fullWidth
                type="date"
                InputProps={{
                  classes: { notchedOutline: classes.noBorder }
                }}
              />
            </Grid>
          </Grid>
          <label className="basicLabel">Nomor NPWP</label>
          <TextField
            variant="outlined"
            margin="normal"
            label={nomorNPWPPasangan === "" ? "Masukkan Nomor NPWP" : ""}
            InputLabelProps={{ shrink: false }}
            defaultValue={nomorNPWPPasangan}
            onChange={(e) => setNomorNPWPPasangan(e.target.value)}
            // required
            fullWidth
            InputProps={{
              classes: { notchedOutline: classes.noBorder }
            }}
          />
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <label className="basicLabel">Nomor Handphone Pasangan</label>
              <TextField
                variant="outlined"
                margin="normal"
                label={nomorTeleponPasangan === "" ? "Masukkan Nomor Handphone" : ""}
                InputLabelProps={{ shrink: false }}
                defaultValue={nomorTeleponPasangan}
                onChange={(e) => setNomorTeleponPasangan(e.target.value)}
                // required
                fullWidth
                InputProps={{
                  classes: { notchedOutline: classes.noBorder }
                }}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <label className="basicLabel">Alamat Email Pasangan</label>
              <TextField
                variant="outlined"
                margin="normal"
                label={emailPasangan === "" ? "Masukkan Alamat Email" : ""}
                InputLabelProps={{ shrink: false }}
                defaultValue={emailPasangan}
                onChange={(e) => setEmailPasangan(e.target.value)}
                // required
                fullWidth
                InputProps={{
                  classes: { notchedOutline: classes.noBorder }
                }}
              />
            </Grid>
          </Grid>
          <div className="firstPageButtonsWrapper">
            <div className="">
              {/* <input
                className="transparentButton"
                type="submit"
                value="Simpan Formulir"
              /> */}
            </div>
            <div className="sliceForSecondPageButton">
              <input
                className="secondaryButton"
                type="submit"
                value="Kembali"
                onClick={() => setStepDataDiri(3.2)}
              />
              <input
                className="primaryButton"
                type="submit"
                value="Lanjut"
                onClick={() => setStepDataDiri(4.2)}
              />
            </div>
          </div>
        </FormControl>

      </Box>
    </Box>
  )
}
