import React, { useState, useContext } from "react";
import { styled } from '@mui/material/styles';
import { Box, Grid, Radio, RadioGroup, FormControlLabel, FormControl, Divider } from '@mui/material'
import '../Styles/styles.css'
import { makeStyles } from "@material-ui/core/styles";
import { TextField, MenuItem } from '@material-ui/core';
import {multiStepContext} from "../StepContext"

const useStyles = makeStyles(() => ({
    noBorder: {
        border: "none",
    }
}));

const BpIcon = styled("span")(({ theme }) => ({
    borderRadius: "50%",
    width: "7px",
    height: "7px",
    border: "1.5px solid #bfbfbf",
    padding: "3px",
    "input:hover ~ &": {
        backgroundColor: "#ebebeb"
    },
    "input:disabled ~ &": {
        boxShadow: "none",
        background:
            theme.palette.mode === "dark"
                ? "rgba(57,75,89,.5)"
                : "rgba(206,217,224,.5)"
    }
}));

const BpCheckedIcon = styled(BpIcon)({
    top: "3.5px",
    left: "3.5px",
    backgroundImage:
        "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
    "&:before": {
        display: "block",
        width: "7.5px",
        height: "7.5px",
        backgroundImage: "radial-gradient(#500878,#500878,#500878)",
        borderRadius: "50%",
        content: '""'
    },
    "input:hover ~ &": {
        // backgroundColor: '#106ba3',
    }
});

// Inspired by blueprintjs
function BpRadio(props) {
    return (
        <Radio
            sx={{
                "&:hover": {
                    bgcolor: "transparent"
                }
            }}
            disableRipple
            color="default"
            checkedIcon={<BpCheckedIcon />}
            icon={<BpIcon />}
            {...props}
        />
    );
}

export default function DataPengajuan() {
    const classes = useStyles();
    const { setStepDataDiri, userData, setUserData } =
    useContext(multiStepContext);

    const [variabel2, setVariabel2] = useState("")
    const [variabel3, setVariabel3] = useState("")
    const [variabel4, setVariabel4] = useState("")


    return (
        <Box sx={{
            width: '80%',
            bgcolor: '#fff',
            alignItems: 'center',
            paddingTop: '48px',
            paddingBottom: '48px',
            borderRadius: '34px'

        }}>
            <Box sx={{
                width: '70%',
                marginLeft: '15%'
            }}>
                <h2 className="titleOne">Form Contoh</h2>
                <Divider
                    sx={{
                        marginTop: '24px',
                        marginBottom: '24px',
                        border: '0, 1px solid #363636',
                        backgroundColor: '#363636'
                    }}
                />
                <h3 className="titleTwo">Form Contoh Menggunakan Mui</h3>

                <FormControl component="fieldset" sx={{ width: '100%' }}>
                    <label className="basicLabel">Label untuk Input Radio</label>
                    <RadioGroup
                        aria-label="gender"
                        name="controlled-radio-buttons-group"
                        sx={{
                            width: '100%'
                        }}
                    >
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={6}>
                                <FormControlLabel value="radio1" control={<BpRadio />}
                                    label={<span style={{ fontFamily: 'Poppins', fontSize: '14px' }}>{"Radio 1"}</span>}
                                    sx={{
                                        backgroundColor: '#f4f4f4',
                                        borderRadius: '8px',
                                        height: '48px',
                                        marginLeft: '0px',
                                        paddingLeft: '0px',
                                        width: '100%',
                                        color: '#363636',
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <FormControlLabel value="radio2" control={<BpRadio />}
                                    label={<span style={{ fontFamily: 'Poppins', fontSize: '14px' }}>{"Radio 2"}</span>}
                                    sx={{
                                        backgroundColor: '#f4f4f4',
                                        borderRadius: '8px',
                                        height: '48px',
                                        marginLeft: '0px',
                                        paddingLeft: '0px',
                                        width: '100%',
                                        color: '#363636'
                                    }}
                                />
                            </Grid>
                        </Grid>
                    </RadioGroup>
                    <label className="basicLabel">Label untuk Dropdown</label>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        label={variabel2 === "" ? "Pilih Menu" : ""}
                        InputLabelProps={{ shrink: false }}
                        defaultValue={variabel2}
                        onChange={(e) => setVariabel2(e.target.value)}
                        // required
                        fullWidth
                        InputProps={{
                            classes: { notchedOutline: classes.noBorder }
                        }}
                        select>
                        <MenuItem value="Value A">Value A</MenuItem>
                        <MenuItem value="Value B">Value B</MenuItem>
                        <MenuItem value="Value C">Value C</MenuItem>
                    </TextField>

                    <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
              <label className="basicLabel">Label Grid 1</label>
                <TextField
                  variant="outlined"
                  margin="normal"
                  label={variabel3 === "" ? "Masukkan Label Grid 1" : ""}
                  InputLabelProps={{ shrink: false }}
                  defaultValue={variabel3}
                  onChange={(e) => setVariabel3(e.target.value)}
                  // required
                  fullWidth
                  InputProps={{
                    classes: { notchedOutline: classes.noBorder }
                  }}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
              <label className="basicLabel">Label Grid 2 dengan helper text</label>
                <TextField
                  variant="outlined"
                  margin="normal"
                  label={variabel4 === "" ? "Masukkan Label Grid 2" : ""}
                  InputLabelProps={{ shrink: false }}
                  defaultValue={variabel4}
                  onChange={(e) => setVariabel4(e.target.value)}
                  // required
                  fullWidth
                  helperText="Nomor Minimal 10 Digit"
                  InputProps={{
                    classes: { notchedOutline: classes.noBorder }
                  }}
                />
              </Grid>
            </Grid>
            <br/>
                    
                    <div className="firstPageButtonsWrapper">
                        <div className="">
                            <input
                                className="transparentButton"
                                type="submit"
                                value="Simpan Formulir"
                            />
                        </div>
                        <div className="sliceForSecondPageButton">
                            <input
                                className="secondaryButton"
                                type="submit"
                                value="Kembali"
                            // onClick={() => setStepDataDiri(3.1)}
                            />
                            <input
                                className="primaryButton"
                                type="submit"
                                value="Lanjut"
                                onClick={() => setStepDataDiri(1.2)}
                            />
                        </div>
                    </div>
                </FormControl>
            </Box>
        </Box>
    )
}
