import React, { useState, useContext } from "react";
import { styled } from '@mui/material/styles';
import {Box, Grid, Radio, RadioGroup, FormControlLabel, FormControl, Divider} from '@mui/material'
import '../Styles/styles.css'
import { makeStyles } from "@material-ui/core/styles";
import { TextField, MenuItem } from '@material-ui/core';
import InputUnstyled from "@mui/core/InputUnstyled";
import {multiStepContext} from "../StepContext"

const StyledInputElement = styled("input")(`
  width: calc(100% - 16px);
  font-family: Poppins;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 24px;
  background: rgb(243, 246, 249);
  border: 0px solid #E5E8EC;
  border-radius: 8px;
  // padding: 6px 10px;
  color: #20262D;
  height: 48px;
  background: #f4f4f4;
  padding-left: 16px;

  &:hover {
    background: #ede5ee;
  }

  &:focus {
    outline: none;
    background: #ede5ee;
  }
`);

const StyledInputWithIconWrapper = styled("div")(`
  position: relative;
  input {
    display: flex;
    flex-direction: row;
    padding-left: 56px;
  }

  label {
    position: absolute;
    left: 0px;
    top: 0px;
    background-color: #e3e3e3;
    height: 36px;
    width: 48px;
    border-radius: 8px 0px 0px 8px;
    text-align: center;
    padding-top: 12px;
    color: #9E9E9E;
  }
  input &:focus + label {
    background-color: #dccee4;
  }
`)

const StyledInputWithIcon = styled("input")(`
  width: calc(100% - 56px);
  font-family: Poppins;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 24px;
  border: 0px solid #E5E8EC;
  border-radius: 8px;
  // padding: 6px 10px;
  color: #20262D;
  height: 48px;
  background: #f4f4f4;
  padding-left: 16px;

  &:hover {
    background: #ede5ee;
  }

  &:focus {
    outline: none;
    background: #ede5ee;
  }
`);

const CustomInput = React.forwardRef(function CustomInput(props, ref) {
  return (
    <InputUnstyled
      components={{ Input: StyledInputElement }}
      {...props}
      ref={ref}
      select
    />
  );
});

const CustomInputWithIcon = React.forwardRef(function CustomInput(props, ref) {
  return (
    <InputUnstyled
      components={{ Input: StyledInputWithIcon }}
      {...props}
      ref={ref}
      select
    />
  );
});

const useStyles = makeStyles(() => ({
  noBorder: {
    border: "none",
  }
}));

const BpIcon = styled("span")(({ theme }) => ({
  borderRadius: "50%",
  width: "7px",
  height: "7px",
  border: "1.5px solid #bfbfbf",
  padding: "3px",
  "input:hover ~ &": {
    backgroundColor: "#ebebeb"
  },
  "input:disabled ~ &": {
    boxShadow: "none",
    background:
      theme.palette.mode === "dark"
        ? "rgba(57,75,89,.5)"
        : "rgba(206,217,224,.5)"
  }
}));

const BpCheckedIcon = styled(BpIcon)({
  top: "3.5px",
  left: "3.5px",
  backgroundImage:
    "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
  "&:before": {
    display: "block",
    width: "7.5px",
    height: "7.5px",
    backgroundImage: "radial-gradient(#500878,#500878,#500878)",
    borderRadius: "50%",
    content: '""'
  },
  "input:hover ~ &": {
    // backgroundColor: '#106ba3',
  }
});

// Inspired by blueprintjs
function BpRadio(props) {
  return (
    <Radio
      sx={{
        "&:hover": {
          bgcolor: "transparent"
        }
      }}
      disableRipple
      color="default"
      checkedIcon={<BpCheckedIcon />}
      icon={<BpIcon />}
      {...props}
    />
  );
}

export default function DataAgunan1() {
  const classes = useStyles();
  // const [value, setValue] = React.useState('female');
  const { setStepDataDiri, userData, setUserData } = useContext(multiStepContext);


  const [peruntukan_pembiayaan, setPeruntukan_Pembiayaan] = useState("")
  const [program, setProgram] = useState("")
  const [status_agunan, setStatus_Agunan] = useState("")
  const [status_kepemilikan, setStatus_Kepemilikan] = useState("")
  const [jenis_agunan, setJenis_Agunan] = useState("")

  return (
    <Box sx={{
      width: '80%',
      bgcolor: '#fff',
      alignItems: 'center',
      paddingTop: '48px',
      paddingBottom: '48px',
      borderRadius: '34px'

    }}>
      <Box sx={{
        width: '70%',
        marginLeft: '15%'
      }}>
        <h2 className="titleOne">Data Agunan</h2>
        <Divider
          sx={{
            marginTop: '24px',
            border: '0, 1px solid #363636',
            backgroundColor: '#363636'
          }}
        />
        
        <FormControl component="fieldset" sx={{ width: '100%' }}>
          
        <label className="basicLabel">Jenis Agunan</label>
          <TextField
            variant="outlined"
            margin="normal"
            label={jenis_agunan === "" ? "Pilih Jenis Agunan" : ""}
            InputLabelProps={{ shrink: false }}
            defaultValue={jenis_agunan}
            onChange={(e) => setJenis_Agunan(e.target.value)}
            // required
            fullWidth
            InputProps={{
              classes: { notchedOutline: classes.noBorder}
            }}
            select
            >
            <MenuItem value="Rumah">Rumah</MenuItem>
            <MenuItem value="Apartemen">Apartemen atau Rusun</MenuItem>
            <MenuItem value="Ruko">Ruko atau Rukan</MenuItem>
            <MenuItem value="Kavling">Kavling</MenuItem>
          </TextField>
          
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <label className="basicLabel">Luas Tanah</label>
              <div className="inputWithIconRightWrapper">
                <input
                  className="inputWithIconRight"
                  type="number"
                  placeholder="0"
                  // onChange={(e) => setLuas_Bangunan(e.target.value)}
                />
                <label className="iconRight">m2</label>
              </div>
            </Grid>
            <Grid item xs={12} sm={6}>
              <label className="basicLabel">Luas Bangunan</label>
              <div className="inputWithIconRightWrapper">
                <input
                  className="inputWithIconRight"
                  type="number"
                  placeholder="0"
                  // onChange={(e) => setLuas_Bangunan(e.target.value)}
                />
                <label className="iconRight">m2</label>
              </div>
            </Grid>
          </Grid>
          
          <label className="basicLabel">Status Kepemilikan</label>
          <TextField
            variant="outlined"
            margin="normal"
            label={status_kepemilikan === "" ? "Pilih Status Kepemilikan" : ""}
            InputLabelProps={{ shrink: false }}
            defaultValue={status_kepemilikan}
            onChange={(e) => setStatus_Kepemilikan(e.target.value)}
            // required
            fullWidth
            InputProps={{
              classes: { notchedOutline: classes.noBorder}
            }}
            select
            >
            <MenuItem value="SHM">SHM</MenuItem>
            <MenuItem value="SHBG">SHBG</MenuItem>
            <MenuItem value="Strata Titel/SHMRS">Strata Titel / SHMRS </MenuItem>
          </TextField>

          <label className="basicLabel">Kondisi Bangunan</label>
          <RadioGroup
            aria-label="gender"
            name="controlled-radio-buttons-group"
            sx={{
              width: '100%'
            }}
          >
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <FormControlLabel value="Siap Huni" control={<BpRadio />}
                  label={<span style={{ fontFamily: 'Poppins', fontSize: '14px' }}>{"Siap Huni"}</span>}
                  sx={{
                    backgroundColor: '#f4f4f4',
                    borderRadius: '8px',
                    height: '48px',
                    marginLeft: '0px',
                    paddingLeft: '0px',
                    width: '100%',
                    color: '#363636',
                  }}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <FormControlLabel value="Dalam Proses Pembangunan" control={<BpRadio />}
                  label={<span style={{ fontFamily: 'Poppins', fontSize: '14px' }}>{"Dalam Proses Pembangunan"}</span>}
                  sx={{
                    backgroundColor: '#f4f4f4',
                    borderRadius: '8px',
                    height: '48px',
                    marginLeft: '0px',
                    paddingLeft: '0px',
                    width: '100%',
                    color: '#363636'
                  }}
                />
              </Grid>
            </Grid>
          </RadioGroup>

          <label className="basicLabel">Status Agunan</label>
          <TextField
            variant="outlined"
            margin="normal"
            label={status_agunan === "" ? "Pilih Status Agunan" : ""}
            InputLabelProps={{ shrink: false }}
            defaultValue={status_agunan}
            onChange={(e) => setStatus_Agunan(e.target.value)}
            // required
            fullWidth
            InputProps={{
              classes: { notchedOutline: classes.noBorder}
            }}
            select
            >
            <MenuItem value="Ditinggali">Ditinggali</MenuItem>
            <MenuItem value="Disewakan">Disewakan</MenuItem>
            <MenuItem value="Kosong">Kosong </MenuItem>
          </TextField>
          
          <label className="basicLabel">Atas Nama Sertifikat</label>
          <CustomInput aria-label="Atas Nama Kepemilikan" placeholder="Masukkan Atas Nama Sertifikat" />
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <label className="basicLabel">Nomor Sertifikat</label>
              <CustomInput aria-label="Atas Nama Kepemilikan" placeholder="Masukkan Nomor Sertifikat" />
            </Grid>
            <Grid item xs={12} sm={6}>
              <label className="basicLabel">Berlaku Hingga</label>
              <CustomInput aria-label="Atas Nama Kepemilikan" placeholder="Masukkan Tanggal Berlaku" type="date"/>
            </Grid>
          </Grid>

          <div className="firstPageButtonsWrapper">
            <div className="">
              {/* <input
                className="transparentButton"
                type="submit"
                value="Simpan Formulir"
                hidden
              /> */}
            </div>
            <div className="sliceForSecondPageButton">
              <input
                className="secondaryButton"
                type="submit"
                value="Kembali"
                onClick={() => setStepDataDiri(2.2)}
              />
              <input
                className="primaryButton"
                type="submit"
                value="Lanjut"
                onClick={() => setStepDataDiri(3.2)}
              />
            </div>
          </div>
        </FormControl>
      </Box>
    </Box>
  )
}
